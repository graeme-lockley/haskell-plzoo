--- Operators - real divide ('/') by 0
PROGRAM Operators;

BEGIN
  WriteLn(1.0 / 0.0)
END.
--- Output
Attempt to divide by zero: Exiting
