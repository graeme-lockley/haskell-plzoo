--- Procedure definition and call with reference arguments
PROGRAM ProcedureReferenceArguments;

VAR
  x0 : INTEGER;
  y0 : REAL;
  z0 : BOOLEAN;

PROCEDURE Proc1(VAR ai1 : INTEGER; bi1 : INTEGER; VAR ar1 : REAL; br1 : REAL; VAR ab1 : BOOLEAN; bb1 : BOOLEAN);
  PROCEDURE Proc2(VAR ai2 : INTEGER; bi2 : INTEGER; VAR ar2 : REAL; br2 : REAL; VAR ab2 : BOOLEAN; bb2 : BOOLEAN);
    BEGIN
      WriteLn("Proc2:");
      WriteLn("  ", x0, " ", y0, " ", z0);
      WriteLn("  ", ai1, " ", ar1, " ", ab1);
      WriteLn("  ", bi1, " ", br1, " ", bb1);
      WriteLn("  ", ai2, " ", ar2, " ", ab2);
      WriteLn("  ", bi2, " ", br2, " ", bb2);

      x0 := x0 + 1;
      y0 := y0 + 1.0;
      z0 := NOT z0;

      ai1 := ai1 + 1;
      ar1 := ar1 + 1.0;
      ab1 := NOT ab1;

      bi1 := bi1 + 1;
      br1 := br1 + 1.0;
      bb1 := NOT bb1;

      ai2 := ai2 + 1;
      ar2 := ar2 + 1.0;
      ab2 := NOT ab2;

      bi2 := bi2 + 1;
      br2 := br2 + 1.0;
      bb2 := NOT bb2;

      WriteLn("Proc2:");
      WriteLn("  ", x0, " ", y0, " ", z0);
      WriteLn("  ", ai1, " ", ar1, " ", ab1);
      WriteLn("  ", bi1, " ", br1, " ", bb1);
      WriteLn("  ", ai2, " ", ar2, " ", ab2);
      WriteLn("  ", bi2, " ", br2, " ", bb2);
    END;

  BEGIN
    WriteLn("Proc1:");
    WriteLn("  ", x0, " ", y0, " ", z0);
    WriteLn("  ", ai1, " ", ar1, " ", ab1);
    WriteLn("  ", bi1, " ", br1, " ", bb1);

    x0 := x0 + 1;
    y0 := y0 + 1.0;
    z0 := NOT z0;

    ai1 := ai1 + 1;
    ar1 := ar1 + 1.0;
    ab1 := NOT ab1;

    bi1 := bi1 + 1;
    br1 := br1 + 1.0;
    bb1 := NOT bb1;

    Proc2(ai1, bi1, ar1, br1, ab1, bb1);

    WriteLn("Proc1:");
    WriteLn("  ", x0, " ", y0, " ", z0);
    WriteLn("  ", ai1, " ", ar1, " ", ab1);
    WriteLn("  ", bi1, " ", br1, " ", bb1);
  END;

BEGIN
  Writeln("main:");
  WriteLn("  ", x0, " ", y0, " ", z0);

  Proc1(x0, x0, y0, y0, z0, z0);

  Writeln("main:");
  WriteLn("  ", x0, " ", y0, " ", z0);
END.
--- Output
main:
  0 0.000000 FALSE
Proc1:
  0 0.000000 FALSE
  0 0.000000 FALSE
  0 0.000000 FALSE
Proc2:
  2 2.000000 FALSE
  2 2.000000 FALSE
  1 1.000000 TRUE
  2 2.000000 FALSE
  1 1.000000 TRUE
Proc2:
  5 5.000000 TRUE
  5 5.000000 TRUE
  2 2.000000 FALSE
  5 5.000000 TRUE
  2 2.000000 FALSE
Proc1:
  5 5.000000 TRUE
  5 5.000000 TRUE
  2 2.000000 FALSE
main:
  5 5.000000 TRUE

