--- Function definition and call without arguments
PROGRAM ArgumentlessFunction;

VAR
  xi : INTEGER;
  xr : REAL;
  xb : BOOLEAN;

FUNCTION FunI(): INTEGER;
  BEGIN
    FunI := xi + 1
  END;

FUNCTION FunR(): REAL;
  BEGIN
    FunR := xr + 1.0
  END;

FUNCTION FunB(): BOOLEAN;
  BEGIN
    FunB := NOT xb
  END;

BEGIN
  xi := 5;
  xr := 10.0;
  xb := TRUE;

  WriteLn(FunI());
  WriteLn(FunR());
  WriteLn(FunB());
END.
--- Output
6
11.000000
FALSE

