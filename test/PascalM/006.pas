--- While statement
PROGRAM WhileStatement;

VAR
  sum : INTEGER;
  lp : INTEGER;

BEGIN
  sum := 0;
  lp := 0;

  WHILE lp < 10 DO
    BEGIN
      lp := lp + 1;
      sum := sum + lp
    END;

  WriteLn("Sum: ", sum)
END.
--- Output
Sum: 55

