--- Procedure definition and call with value arguments
PROGRAM ProcedureValueArguments;

VAR
  x0 : INTEGER;
  y0 : REAL;
  z0 : BOOLEAN;

PROCEDURE Proc1(ai1: INTEGER; ar1: REAL; ab1: BOOLEAN);
  PROCEDURE Proc2(ai2: INTEGER; ar2: REAL; ab2: BOOLEAN);
    BEGIN
      WriteLn("Proc2: ", x0, " ", y0, " ", z0, " : ", ai1, " ", ar1, " ", ab1, " : ", ai2, " ", ar2, " ", ab2);

      x0 := x0 + 1;
      y0 := y0 + 1.0;
      z0 := NOT z0;

      ai1 := ai1 - 1;
      ar1 := ar1 + 1.0;
      ab1 := NOT ab1;

      ai2 := ai2 - 1;
      ar2 := ar2 + 1.0;
      ab2 := NOT ab2;

      WriteLn("Proc2: ", x0, " ", y0, " ", z0, " : ", ai1, " ", ar1, " ", ab1, " : ", ai2, " ", ar2, " ", ab2);
    END;

  BEGIN
    WriteLn("Proc1: ", x0, " ", y0, " ", z0, " : ", ai1, " ", ar1, " ", ab1);

    x0 := x0 + 1;
    y0 := y0 + 1.0;
    z0 := NOT z0;

    ai1 := ai1 - 1;
    ar1 := ar1 + 1.0;
    ab1 := NOT ab1;

    Proc2(ai1 - 1, ar1 + 1.5, NOT ab1);

    WriteLn("Proc1: ", x0, " ", y0, " ", z0, " : ", ai1, " ", ar1, " ", ab1);
  END;

BEGIN
  Writeln("main: ", x0, " ", y0, " ", z0);
  Proc1(x0 - 1, y0 + 1.5, NOT z0);
  Writeln("main: ", x0, " ", y0, " ", z0)
END.
--- Output
main: 0 0.000000 FALSE
Proc1: 0 0.000000 FALSE : -1 1.500000 TRUE
Proc2: 1 1.000000 TRUE : -2 2.500000 FALSE : -3 4.000000 TRUE
Proc2: 2 2.000000 FALSE : -3 3.500000 TRUE : -4 5.000000 FALSE
Proc1: 2 2.000000 FALSE : -3 3.500000 TRUE
main: 2 2.000000 FALSE

