--- Function definition and call without arguments
PROGRAM ValueAndReferenceParameterFunction;

VAR
  xi : INTEGER;
  xr : REAL;
  xb : BOOLEAN;

FUNCTION FunI(VAR iv1: INTEGER; ir1: INTEGER): INTEGER;
  BEGIN
    WriteLn("FunI:");
    WriteLn("  ", xi, " ", iv1, " ", ir1);

    xi := xi + 1;
    iv1 := iv1 + 1;
    ir1 := ir1 + 1;

    FunI := xi + iv1 + ir1 + 1;

    WriteLn("FunI:");
    WriteLn("  ", xi, " ", iv1, " ", ir1);
  END;

FUNCTION FunR(VAR rv1: REAL; rr1: REAL): REAL;
  BEGIN
    WriteLn("FunR:");
    WriteLn("  ", xr, " ", rv1, " ", rr1);

    xr := xr + 1.0;
    rv1 := rv1 + 1.0;
    rr1 := rr1 + 1.0;

    FunR := xr + rv1 + rr1 + 1.0;

    WriteLn("FunR:");
    WriteLn("  ", xr, " ", rv1, " ", rr1);
  END;

FUNCTION FunB(VAR bv1: BOOLEAN; br1: BOOLEAN): BOOLEAN;
  BEGIN
    WriteLn("FunB:");
    WriteLn("  ", xb, " ", bv1, " ", br1);

    xb := NOT xb;
    bv1 := bv1 OR br1;
    br1 := bv1 AND br1;

    FunB := NOT xb;

    WriteLn("FunB:");
    WriteLn("  ", xb, " ", bv1, " ", br1);
  END;

BEGIN
  xi := 5;
  xr := 10.0;
  xb := TRUE;

  WriteLn("main:");
  WriteLn("  ", xi, " ", xr, " ", xb);

  WriteLn(FunI(xi, xi + 1));
  WriteLn(FunR(xr, xr + 1.0));
  WriteLn(FunB(xb, NOT xb));

  WriteLn("main:");
  WriteLn("  ", xi, " ", xr, " ", xb);
END.
--- Output
main:
  5 10.000000 TRUE
FunI:
  5 5 6
FunI:
  7 7 7
22
FunR:
  10.000000 10.000000 11.000000
FunR:
  12.000000 12.000000 12.000000
37.000000
FunB:
  TRUE TRUE FALSE
FunB:
  FALSE FALSE FALSE
TRUE
main:
  7 12.000000 FALSE

