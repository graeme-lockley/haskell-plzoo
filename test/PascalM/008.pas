--- Procedure definition and calls with nested declarations
PROGRAM ProcedureVariables;

VAR
  x0 : INTEGER;
  y0 : REAL;
  z0 : BOOLEAN;

PROCEDURE Proc1();
  VAR
    x1 : INTEGER;
    y1 : REAL;
    z1 : BOOLEAN;

  PROCEDURE Proc2();
    VAR
      x2 : INTEGER;
      y2 : REAL;
      z2 : BOOLEAN;
    BEGIN
      WriteLn("Proc2: ", x0, " ", y0, " ", z0, " : ", x1, " ", y1, " ", z1, " : ", x2, " ", y2, " ", z2);

      x0 := x0 + 1;
      y0 := y0 + 1.0;
      z0 := NOT z0;

      x1 := x1 + 1;
      y1 := y1 + 1.0;
      z1 := NOT z1;

      x2 := 1;
      y2 := 3.5;
      z2 := NOT z2;

      WriteLn("Proc2: ", x0, " ", y0, " ", z0, " : ", x1, " ", y1, " ", z1, " : ", x2, " ", y2, " ", z2);
    END;

  BEGIN
    WriteLn("Proc1: ", x0, " ", y0, " ", z0, " : ", x1, " ", y1, " ", z1);

    x0 := x0 + 1;
    y0 := y0 + 1.0;
    z0 := NOT z0;

    x1 := 1;
    y1 := 3.5;
    z1 := NOT z1;

    Proc2();
    WriteLn("Proc1: ", x0, " ", y0, " ", z0, " : ", x1, " ", y1, " ", z1);
  END;

BEGIN
  Writeln("main: ", x0, " ", y0, " ", z0);
  Proc1();
  Writeln("main: ", x0, " ", y0, " ", z0)
END.
--- Output
main: 0 0.000000 FALSE
Proc1: 0 0.000000 FALSE : 0 0.000000 FALSE
Proc2: 1 1.000000 TRUE : 1 3.500000 TRUE : 0 0.000000 FALSE
Proc2: 2 2.000000 FALSE : 2 4.500000 FALSE : 1 3.500000 TRUE
Proc1: 2 2.000000 FALSE : 2 4.500000 FALSE
main: 2 2.000000 FALSE

