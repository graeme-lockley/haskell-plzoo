--- Operators - unary and binary
PROGRAM Operators;

BEGIN
  WriteLn("Addition");
  WriteLn("  Integer: ", 1 + 1);
  WriteLn("  Real: ", 1.0 + 1.0);

  WriteLn("Subtraction");
  WriteLn("  Integer: ", 10 - 2);
  WriteLn("  Real: ", 10.0 - 2.0);

  WriteLn("Multiplication");
  WriteLn("  Integer: ", 11 * 3);
  WriteLn("  Real: ", 11.0 * 3.0);

  WriteLn("Div");
  WriteLn("  Integer: ", 16 div 3);

  WriteLn("Mod");
  WriteLn("  Integer: ", 16 mod 3);

  WriteLn("Divide");
  WriteLn("  Integer: ", 16 / 3);
  WriteLn("  Real: ", 16.0 / 3.0);

  WriteLn("Equal");
  WriteLn("  Integer #1: ", 1 = 1);
  WriteLn("  Integer #2: ", 1 = 0);
  WriteLn("  Real #1: ", 1.0 = 1.0);
  WriteLn("  Real #2: ", 1.0 = 0.0);
  WriteLn("  Boolean #1: ", True = True);
  WriteLn("  Boolean #2: ", True = False);

  WriteLn("Not Equal");
  WriteLn("  Integer #1: ", 1 # 1);
  WriteLn("  Integer #2: ", 1 # 0);
  WriteLn("  Real #1: ", 1.0 # 1.0);
  WriteLn("  Real #2: ", 1.0 # 0.0);
  WriteLn("  Boolean #1: ", True # True);
  WriteLn("  Boolean #2: ", True # False);

  WriteLn("Less Than");
  WriteLn("  Integer: ", 0 < 1);
  WriteLn("  Real: ", 0.0 < 1.0);
  WriteLn("  Boolean: ", True < False);

  WriteLn("Less Equal");
  WriteLn("  Integer: ", 0 <= 1);
  WriteLn("  Real: ", 0.0 <= 1.0);
  WriteLn("  Boolean: ", True <= False);

  WriteLn("Greater Than");
  WriteLn("  Integer: ", 0 > 1);
  WriteLn("  Real: ", 0.0 > 1.0);
  WriteLn("  Boolean: ", True > False);

  WriteLn("Greater Equal");
  WriteLn("  Integer: ", 0 >= 1);
  WriteLn("  Real: ", 0.0 >= 1.0);
  WriteLn("  Boolean: ", True >= False);

  WriteLn("And");
  WriteLn("  TRUE and TRUE: ", TRUE and TRUE);
  WriteLn("  TRUE and FALSE: ", TRUE and FALSE);
  WriteLn("  FALSE and TRUE: ", FALSE and TRUE);
  WriteLn("  FALSE and FALSE: ", FALSE and FALSE);

  WriteLn("Or");
  WriteLn("  TRUE or TRUE: ", TRUE or TRUE);
  WriteLn("  TRUE or FALSE: ", TRUE or FALSE);
  WriteLn("  FALSE or TRUE: ", FALSE or TRUE);
  WriteLn("  FALSE or FALSE: ", FALSE or FALSE);

  WriteLn("Not");
  WriteLn("  TRUE: ", Not TRUE);
  WriteLn("  FALSE: ", Not FALSE);

  WriteLn("Negate");
  WriteLn("  Integer #1: ", -(10));
  WriteLn("  Integer #2: ", -(-10));
  WriteLn("  Real #1: ", -(10.0));
  WriteLn("  Real #2: ", -(-10.0));
  WriteLn("  Real #3: ", -0.0);
END.
--- Output
Addition
  Integer: 2
  Real: 2.000000
Subtraction
  Integer: 8
  Real: 8.000000
Multiplication
  Integer: 33
  Real: 33.000000
Div
  Integer: 5
Mod
  Integer: 1
Divide
  Integer: 5.333333
  Real: 5.333333
Equal
  Integer #1: TRUE
  Integer #2: FALSE
  Real #1: TRUE
  Real #2: FALSE
  Boolean #1: TRUE
  Boolean #2: FALSE
Not Equal
  Integer #1: FALSE
  Integer #2: TRUE
  Real #1: FALSE
  Real #2: TRUE
  Boolean #1: FALSE
  Boolean #2: TRUE
Less Than
  Integer: TRUE
  Real: TRUE
  Boolean: FALSE
Less Equal
  Integer: TRUE
  Real: TRUE
  Boolean: FALSE
Greater Than
  Integer: FALSE
  Real: FALSE
  Boolean: TRUE
Greater Equal
  Integer: FALSE
  Real: FALSE
  Boolean: TRUE
And
  TRUE and TRUE: TRUE
  TRUE and FALSE: FALSE
  FALSE and TRUE: FALSE
  FALSE and FALSE: FALSE
Or
  TRUE or TRUE: TRUE
  TRUE or FALSE: TRUE
  FALSE or TRUE: TRUE
  FALSE or FALSE: FALSE
Not
  TRUE: FALSE
  FALSE: TRUE
Negate
  Integer #1: -10
  Integer #2: 10
  Real #1: -10.000000
  Real #2: 10.000000
  Real #3: -0.000000

