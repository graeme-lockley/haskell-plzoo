--- Recursive function
PROGRAM RecursiveFunction;

FUNCTION fact(n: INTEGER): INTEGER;
  BEGIN
    IF n < 1 THEN
      fact := 1
    ELSE
      fact := n * fact(n-1)
  END;

BEGIN
  WriteLn(fact(0));
  WriteLn(fact(1));
  WriteLn(fact(2));
  WriteLn(fact(3));
  WriteLn(fact(4));
END.
--- Output
1
1
2
6
24

