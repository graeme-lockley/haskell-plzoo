--- Global variable declaration
PROGRAM GlobalVariables;

VAR
  x : INTEGER;
  r : REAL;
  b : BOOLEAN;

BEGIN
  WriteLn("Integer: ", x);
  WriteLn("Real: ", r);
  WriteLn("Boolean: ", b);

  x := x + 1;
  r := r + 1.0;
  b := b Or True;

  WriteLn("Integer: ", x);
  WriteLn("Real: ", r);
  WriteLn("Boolean: ", b);
END.
--- Output
Integer: 0
Real: 0.000000
Boolean: FALSE
Integer: 1
Real: 1.000000
Boolean: TRUE

