--- If-Then-Else statement
PROGRAM IfThenElse;

VAR
  x : INTEGER;

BEGIN
  x := 10;

  IF x < 10 THEN
    WriteLn("Silent")
  ELSE
    BEGIN
      x := x + 5;
      Writeln("I am here");
    END;

  IF x < 100 THEN
    WriteLn("Integer: ", x)
  ELSE
    WriteLn("Silent")
END.
--- Output
I am here
Integer: 15

