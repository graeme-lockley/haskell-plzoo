#include <stdio.h>

void _write_string(char *s) {
  printf("%s", s);
}

void _write_ln() {
  printf("\n");
}

void _write_int(int v) {
  printf("%d", v);
}

void _write_bool(int v) {
  if (v == 1) printf("TRUE"); else printf("FALSE");
}

void _write_real(float v) {
  printf("%f", v);
}

int _int_div(int m, int n) {
  if (n == 0) {
    fprintf(stderr, "Attempt to divide by zero: Exiting");
    exit(-1);
  }

  return (int) m / n;
}

float _int_divide(int m, int n) {
  if (n == 0) {
    fprintf(stderr, "Attempt to divide by zero: Exiting");
    exit(-1);
  }

  return (float) m / n;
}

int _int_mod(int m, int n) {
  if (n == 0) {
    fprintf(stderr, "Attempt to modulo zero: Exiting");
    exit(-1);
  }

  return (int) m % n;
}


int _real_divide(float m, float n) {
  if (n == 0.0) {
    fprintf(stderr, "Attempt to divide by zero: Exiting");
    exit(-1);
  }

  return m / n;
}

