--- Operators - integer divide (div) by 0
PROGRAM Operators;

BEGIN
  WriteLn(1 div 0)
END.
--- Output
Attempt to divide by zero: Exiting
