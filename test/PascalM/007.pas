--- Procedure definition and call without arguments
PROGRAM ArgumentlessProcedure;

VAR
  x : INTEGER;

PROCEDURE Proc();
BEGIN
  WriteLn("Hello: ", x)
END;

BEGIN
  x := 5;
  Proc();
  x := 10;
  Proc()
END.
--- Output
Hello: 5
Hello: 10

