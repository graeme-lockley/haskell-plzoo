--- If-Then statement
PROGRAM IfThen;

VAR
  x : INTEGER;

BEGIN
  x := 10;

  IF x < 10 THEN
    WriteLn("Silent");

  IF x >= 10 THEN
    BEGIN
      x := x + 5;
      Writeln("I am here");
    END;

  WriteLn("Integer: ", x);
END.
--- Output
I am here
Integer: 15

