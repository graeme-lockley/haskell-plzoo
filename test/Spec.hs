import           Control.Monad.State
import           Data.List                (intercalate, isPrefixOf)
import qualified Data.Map.Strict          as Map
import           GHC.IO.Exception         (ExitCode (..))
import qualified System.IO                as IO
import qualified System.Process           as Process
import           Test.Hspec
import qualified Text.PrettyPrint         as PP

import qualified Lang.PascalM.AST         as AST
import           Lang.PascalM.Compiler    (compile)
import qualified Lang.PascalM.Errors      as Errors
import qualified Lang.PascalM.IR          as IR
import           Lang.PascalM.Parser      (parse)
import qualified Lang.PascalM.TST         as TST
import           Lang.PascalM.TypeChecker (Scope, emptyScope, insertIdentifier,
                                           insertStandardIdentifier,
                                           typeCheckDeclaration,
                                           typeCheckExpression,
                                           typeCheckProgram, typeCheckStatement,
                                           typeCheckVariableDeclaration)
import           Lang.PascalM.Types

main :: IO ()
main =
  hspec $
  describe "Lang.PascalM" $ do
    describe "Static Semantics" $ do
      describe "Expression" $ do
        describe "Literal Boolean" $ do
          it "True [] returns Boolean" $ tcExpr (AST.LiteralBoolean rspA True) emptyScope `shouldBe` (Just TBoolean, [])
          it "False [] returns Boolean" $ tcExpr (AST.LiteralBoolean rspA False) emptyScope `shouldBe` (Just TBoolean, [])
        describe "Literal Integer" $ it "123 [] returns Integer" $ tcExpr (AST.LiteralInteger rspA 123) emptyScope `shouldBe` (Just TInteger, [])
        describe "Literal Real" $ it "3.1415 [] returns Real" $ tcExpr (AST.LiteralReal rspA 3.1415) emptyScope `shouldBe` (Just TReal, [])
        describe "Literal String" $
          it "\"Hello World\" returns String" $ tcExpr (AST.LiteralString rspA "Hello World") emptyScope `shouldBe` (Just TString, [])
        describe "Identifier Reference" $ do
          it "x [] returns an UnknownIdentifier error" $
            tcExpr (AST.IdentifierReference (AST.Identifier rspA "x")) emptyScope `shouldBe` (Nothing, [Errors.UnknownIdentifier rspA "x"])
          it "x [x -> Bool] returns ^Bool" $
            tcExpr (AST.IdentifierReference (AST.Identifier rspA "x")) (insertStandardIdentifier "x" TBoolean emptyScope) `shouldBe`
            (Just $ TPointerTo TBoolean, [])
        describe "Not" $ do
          it "not True returns Boolean" $ tcExpr (AST.Not rspA (AST.LiteralBoolean rspB True)) emptyScope `shouldBe` (Just TBoolean, [])
          it "not 123 returns NotRequiresBooleanOperand" $
            tcExpr (AST.Not rspA (AST.LiteralInteger rspB 123)) emptyScope `shouldBe` (Nothing, [Errors.NotRequiresBooleanOperand rspA TInteger])
        describe "Unary '-'" $ do
          it "-123 returns Integer" $ tcExpr (AST.Negate rspA (AST.LiteralInteger rspB 123)) emptyScope `shouldBe` (Just TInteger, [])
          it "-123.456 returns Real" $ tcExpr (AST.Negate rspA (AST.LiteralReal rspB 123.123)) emptyScope `shouldBe` (Just TReal, [])
          it "-True returns NegateRequiresNumericOperand" $
            tcExpr (AST.Negate rspA (AST.LiteralBoolean rspB True)) emptyScope `shouldBe`
            (Nothing, [Errors.NegateRequiresNumericOperand rspA TBoolean])
        describe "Arithmetic Binary Op" $ do
          it "1 + 2 returns Integer" $
            tcExpr (AST.ArithOp AST.Plus (AST.LiteralInteger rspA 1) (AST.LiteralInteger rspB 2)) emptyScope `shouldBe` (Just TInteger, [])
          it "1 + 2.0 returns ArithOperatorsMismatch" $
            tcExpr (AST.ArithOp AST.Plus (AST.LiteralInteger rspA 1) (AST.LiteralReal rspB 2.0)) emptyScope `shouldBe`
            (Nothing, [Errors.ArithmeticOperandsMismatch rspAB TInteger TReal])
          it "1.0 + 2 returns ArithOperatorsMismatch" $
            tcExpr (AST.ArithOp AST.Plus (AST.LiteralReal rspA 1.0) (AST.LiteralInteger rspB 2)) emptyScope `shouldBe`
            (Nothing, [Errors.ArithmeticOperandsMismatch rspAB TReal TInteger])
          it "1.0 + 2.0 returns Real" $
            tcExpr (AST.ArithOp AST.Plus (AST.LiteralReal rspA 1.0) (AST.LiteralReal rspB 2.0)) emptyScope `shouldBe` (Just TReal, [])
          it "True + False returns ArithOperandsNotNumbers" $
            tcExpr (AST.ArithOp AST.Plus (AST.LiteralBoolean rspA True) (AST.LiteralBoolean rspB False)) emptyScope `shouldBe`
            (Nothing, [Errors.ArithmeticOperandsNotNumbers rspAB TBoolean])
          it "1 + x [x -> ^Integer] returns Integer" $
            tcExpr
              (AST.ArithOp AST.Plus (AST.LiteralInteger rspA 1) (AST.IdentifierReference (AST.Identifier rspB "x")))
              (insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            (Just TInteger, [])
          it "1 + x [x -> ^Boolean] returns ArithOperatorsMismatch" $
            tcExpr
              (AST.ArithOp AST.Plus (AST.LiteralInteger rspA 1) (AST.IdentifierReference (AST.Identifier rspB "x")))
              (insertStandardIdentifier "x" TBoolean emptyScope) `shouldBe`
            (Nothing, [Errors.ArithmeticOperandsMismatch rspAB TInteger TBoolean])
        describe "Boolean Binary Op" $ do
          it "TRUE AND FALSE returns Boolean" $
            tcExpr (AST.BoolOp AST.And (AST.LiteralBoolean rspA True) (AST.LiteralBoolean rspB False)) emptyScope `shouldBe` (Just TBoolean, [])
          it "TRUE AND 2.0 returns BooleanOperatorsMismatch" $
            tcExpr (AST.BoolOp AST.And (AST.LiteralBoolean rspA True) (AST.LiteralReal rspB 2.0)) emptyScope `shouldBe`
            (Nothing, [Errors.BooleanOperandsMismatch rspAB TBoolean TReal])
          it "1 AND TRUE returns BooleanOperatorsMismatch" $
            tcExpr (AST.BoolOp AST.And (AST.LiteralInteger rspA 1) (AST.LiteralBoolean rspB True)) emptyScope `shouldBe`
            (Nothing, [Errors.BooleanOperandsMismatch rspAB TInteger TBoolean])
          it "TRUE AND x [x -> Boolean] returns Boolean" $
            tcExpr
              (AST.BoolOp AST.And (AST.LiteralBoolean rspA True) (AST.IdentifierReference (AST.Identifier rspB "x")))
              (insertStandardIdentifier "x" TBoolean emptyScope) `shouldBe`
            (Just TBoolean, [])
          it "x AND TRUE [x -> Boolean] returns Boolean" $
            tcExpr
              (AST.BoolOp AST.And (AST.IdentifierReference (AST.Identifier rspA "x")) (AST.LiteralBoolean rspB True))
              (insertStandardIdentifier "x" TBoolean emptyScope) `shouldBe`
            (Just TBoolean, [])
          it "TRUE AND x [x -> Integer] returns BooleanOperatorsMismatch" $
            tcExpr
              (AST.BoolOp AST.And (AST.LiteralBoolean rspA True) (AST.IdentifierReference (AST.Identifier rspAB "x")))
              (insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            (Nothing, [Errors.BooleanOperandsMismatch rspAB TBoolean TInteger])
          it "x AND TRUE [x -> Real] returns BooleanOperatorsMismatch" $
            tcExpr
              (AST.BoolOp AST.And (AST.IdentifierReference (AST.Identifier rspA "x")) (AST.LiteralBoolean rspB True))
              (insertStandardIdentifier "x" TReal emptyScope) `shouldBe`
            (Nothing, [Errors.BooleanOperandsMismatch rspAB TReal TBoolean])
        describe "Comparison Binary Op" $ do
          it "TRUE = FALSE returns Boolean" $
            tcExpr (AST.CompOp AST.Equal (AST.LiteralBoolean rspA True) (AST.LiteralBoolean rspB False)) emptyScope `shouldBe` (Just TBoolean, [])
          it "1 = 2 returns Boolean" $
            tcExpr (AST.CompOp AST.Equal (AST.LiteralInteger rspA 1) (AST.LiteralInteger rspB 2)) emptyScope `shouldBe` (Just TBoolean, [])
          it "1.0 = 2.0 returns Boolean" $
            tcExpr (AST.CompOp AST.Equal (AST.LiteralReal rspA 1.0) (AST.LiteralReal rspB 2.0)) emptyScope `shouldBe` (Just TBoolean, [])
          it "TRUE = 2.0 returns ComparisonOperatorsMismatch" $
            tcExpr (AST.CompOp AST.Equal (AST.LiteralBoolean rspA True) (AST.LiteralReal rspB 2.0)) emptyScope `shouldBe`
            (Nothing, [Errors.ComparisonOperandsMismatch rspAB TBoolean TReal])
          it "1.0 = x [x -> Real] returns Boolean" $
            tcExpr
              (AST.CompOp AST.Equal (AST.LiteralReal rspA 1.0) (AST.IdentifierReference (AST.Identifier rspA "x")))
              (insertStandardIdentifier "x" TReal emptyScope) `shouldBe`
            (Just TBoolean, [])
          it "x = 2.0 [x -> Real] returns Boolean" $
            tcExpr
              (AST.CompOp AST.Equal (AST.IdentifierReference (AST.Identifier rspA "x")) (AST.LiteralReal rspB 2.0))
              (insertStandardIdentifier "x" TReal emptyScope) `shouldBe`
            (Just TBoolean, [])
          it "1 = x [x -> Real] returns ComparisonOperatorsMismatch" $
            tcExpr
              (AST.CompOp AST.Equal (AST.LiteralInteger rspA 1) (AST.IdentifierReference (AST.Identifier rspB "x")))
              (insertStandardIdentifier "x" TReal emptyScope) `shouldBe`
            (Nothing, [Errors.ComparisonOperandsMismatch rspAB TInteger TReal])
          it "x = 2 [x -> Real] returns ComparisonOperatorsMismatch" $
            tcExpr
              (AST.CompOp AST.Equal (AST.IdentifierReference (AST.Identifier rspA "x")) (AST.LiteralInteger rspB 2))
              (insertStandardIdentifier "x" TReal emptyScope) `shouldBe`
            (Nothing, [Errors.ComparisonOperandsMismatch rspAB TReal TInteger])
        describe "Function call" $ do
          it "ff() [] returns UnknownIdentifier" $
            tcExpr (AST.Function rspA (AST.Identifier rspB "ff") []) emptyScope `shouldBe` (Nothing, [Errors.UnknownIdentifier rspB "ff"])
          it "ff() [ff -> Integer] returns AttemptToCallVariableAsFunction" $
            tcExpr (AST.Function rspA (AST.Identifier rspB "ff") []) (insertStandardIdentifier "ff" TInteger emptyScope) `shouldBe`
            (Nothing, [Errors.AttemptToCallVariableAsFunction rspB "ff"])
          it "ff() [ff -> Procedure []] returns AttemptToCallProcedureAsFunction" $
            tcExpr (AST.Function rspA (AST.Identifier rspB "ff") []) (insertIdentifier "ff" (DProcedure []) emptyScope) `shouldBe`
            (Nothing, [Errors.AttemptToCallProcedureAsFunction rspB "ff"])
          it "ff() [ff -> Function [] Integer] returns Integer" $
            tcExpr (AST.Function rspA (AST.Identifier rspB "ff") []) (insertIdentifier "ff" (DFunction [] TInteger) emptyScope) `shouldBe`
            (Just TInteger, [])
          it "ff(1) [ff -> Function [] Integer] returns Integer and IncorrectNumberOfArguments" $
            tcExpr
              (AST.Function rspA (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1])
              (insertIdentifier "ff" (DFunction [] TInteger) emptyScope) `shouldBe`
            (Just TInteger, [Errors.IncorrectNumberOfArguments rspA "ff" 0 1])
          it "ff(1, 2.0, TRUE) [ff -> Function [Integer, Real, Boolean] Integer] returns Integer" $
            tcExpr
              (AST.Function rspA (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1, AST.LiteralReal rspC 2.0, AST.LiteralBoolean rspC True])
              (insertIdentifier "ff" (DFunction [TInteger, TReal, TBoolean] TInteger) emptyScope) `shouldBe`
            (Just TInteger, [])
          it "ff(1, 2.0, TRUE) [ff -> Function [Real, Boolean, Integer] Integer] returns (Integer, 3*errors)" $
            tcExpr
              (AST.Function rspA (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1, AST.LiteralReal rspC 2.0, AST.LiteralBoolean rspC True])
              (insertIdentifier "ff" (DFunction [TReal, TBoolean, TInteger] TInteger) emptyScope) `shouldBe`
            ( Just TInteger
            , [ Errors.ArgumentHasIncorrectType rspC "ff" 0 TReal TInteger
              , Errors.ArgumentHasIncorrectType rspC "ff" 1 TBoolean TReal
              , Errors.ArgumentHasIncorrectType rspC "ff" 2 TInteger TBoolean
              ])
          it "ff(x) [ff -> Function [^Integer] Integer, x -> Integer] returns Integer" $
            tcExpr
              (AST.Function rspA (AST.Identifier rspB "ff") [AST.IdentifierReference (AST.Identifier rspC "x")])
              (insertIdentifier "ff" (DFunction [TPointerTo TInteger] TInteger) $ insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            (Just TInteger, [])
          it "ff(x) [ff -> Function [Integer] Integer, x -> Integer] returns Integer" $
            tcExpr
              (AST.Function rspA (AST.Identifier rspB "ff") [AST.IdentifierReference (AST.Identifier rspC "x")])
              (insertIdentifier "ff" (DFunction [TInteger] TInteger) $ insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            (Just TInteger, [])
      describe "Statement" $ do
        it "Empty returns no errors" $ tcStatement AST.Empty emptyScope `shouldBe` []
        describe "if-then" $ do
          it "IF True Then Blank returns no errors" $ tcStatement (AST.IfThen (AST.LiteralBoolean rspA True) AST.Empty) emptyScope `shouldBe` []
          it "IF 1 Then Blank returns IfExpressionNotBoolean" $
            tcStatement (AST.IfThen (AST.LiteralInteger rspA 1) AST.Empty) emptyScope `shouldBe` [Errors.IfExpressionNotBoolean rspA TInteger]
          it "IF 1.0 Then Blank returns IfExpressionNotBoolean" $
            tcStatement (AST.IfThen (AST.LiteralReal rspA 1.0) AST.Empty) emptyScope `shouldBe` [Errors.IfExpressionNotBoolean rspA TReal]
        describe "if-then-else" $ do
          it "IF True Then Blank Else Blank returns no errors" $
            tcStatement (AST.IfThenElse (AST.LiteralBoolean rspA True) AST.Empty AST.Empty) emptyScope `shouldBe` []
          it "IF 1 Then Blank Else Blank  returns IfExpressionNotBoolean" $
            tcStatement (AST.IfThenElse (AST.LiteralInteger rspA 1) AST.Empty AST.Empty) emptyScope `shouldBe`
            [Errors.IfExpressionNotBoolean rspA TInteger]
          it "IF 1.0 Then Blank Else Blank  returns IfExpressionNotBoolean" $
            tcStatement (AST.IfThenElse (AST.LiteralReal rspA 1.0) AST.Empty AST.Empty) emptyScope `shouldBe`
            [Errors.IfExpressionNotBoolean rspA TReal]
        describe "while" $ do
          it "WHILE True DO Blank returns no errors" $ tcStatement (AST.While (AST.LiteralBoolean rspA True) AST.Empty) emptyScope `shouldBe` []
          it "WHILE 1 DO Blank returns IfExpressionNotBoolean" $
            tcStatement (AST.While (AST.LiteralInteger rspA 1) AST.Empty) emptyScope `shouldBe` [Errors.WhileExpressionNotBoolean rspA TInteger]
          it "WHILE 1.0 DO Blank returns IfExpressionNotBoolean" $
            tcStatement (AST.While (AST.LiteralReal rspA 1.0) AST.Empty) emptyScope `shouldBe` [Errors.WhileExpressionNotBoolean rspA TReal]
        describe "assignment" $ do
          it "a := 1 [a -> Integer] returns no errors" $
            tcStatement (AST.Assignment (AST.Identifier rspA "a") (AST.LiteralInteger rspB 1)) (insertStandardIdentifier "a" TInteger emptyScope) `shouldBe`
            []
          it "a := 1 [] returns UnknownIdentifier" $
            tcStatement (AST.Assignment (AST.Identifier rspA "a") (AST.LiteralInteger rspB 1)) emptyScope `shouldBe`
            [Errors.UnknownIdentifier rspA "a"]
          it "a := 1 [a -> Real] returns IncompatibleAssignment" $
            tcStatement (AST.Assignment (AST.Identifier rspA "a") (AST.LiteralInteger rspB 1)) (insertStandardIdentifier "a" TReal emptyScope) `shouldBe`
            [Errors.IncompatibleAssignment rspA TReal rspB TInteger]
          it "a := 1.0 [a -> Integer] returns IncompatibleAssignment" $
            tcStatement (AST.Assignment (AST.Identifier rspA "a") (AST.LiteralReal rspB 1.0)) (insertStandardIdentifier "a" TInteger emptyScope) `shouldBe`
            [Errors.IncompatibleAssignment rspA TInteger rspB TReal]
          it "a := b [a -> Integer, b -> Integer] returns no errors" $
            tcStatement
              (AST.Assignment (AST.Identifier rspA "a") (AST.IdentifierReference (AST.Identifier rspB "b")))
              (insertStandardIdentifier "a" TInteger $ insertStandardIdentifier "b" TInteger emptyScope) `shouldBe`
            []
          it "a := b [a -> Integer, b -> Real] returns IncompatibleAssignment" $
            tcStatement
              (AST.Assignment (AST.Identifier rspA "a") (AST.IdentifierReference (AST.Identifier rspB "b")))
              (insertStandardIdentifier "a" TInteger $ insertStandardIdentifier "b" TReal emptyScope) `shouldBe`
            [Errors.IncompatibleAssignment rspA TInteger rspB TReal]
        describe "procedure call" $ do
          it "ff() [] returns UnknownIdentifier" $
            tcStatement (AST.Call (AST.Identifier rspB "ff") []) emptyScope `shouldBe` [Errors.UnknownIdentifier rspB "ff"]
          it "ff() [ff -> Integer] returns AttemptToCallVariableAsFunction" $
            tcStatement (AST.Call (AST.Identifier rspB "ff") []) (insertStandardIdentifier "ff" TInteger emptyScope) `shouldBe`
            [Errors.AttemptToCallVariableAsProcedure rspB "ff"]
          it "ff() [ff -> Function [] Integer] returns AttemptToCallFunctionAsProcedure" $
            tcStatement (AST.Call (AST.Identifier rspB "ff") []) (insertIdentifier "ff" (DFunction [] TInteger) emptyScope) `shouldBe`
            [Errors.AttemptToCallFunctionAsProcedure rspB "ff"]
          it "ff() [ff -> Procedure []] returns no errors" $
            tcStatement (AST.Call (AST.Identifier rspB "ff") []) (insertIdentifier "ff" (DProcedure []) emptyScope) `shouldBe` []
          it "ff(1) [ff -> Procedure []] returns IncorrectNumberOfArguments" $
            tcStatement (AST.Call (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1]) (insertIdentifier "ff" (DProcedure []) emptyScope) `shouldBe`
            [Errors.IncorrectNumberOfArguments rspB "ff" 0 1]
          it "ff(1, 2.0, TRUE) [ff -> Procedure [Integer, Real, Boolean]] returns no errors" $
            tcStatement
              (AST.Call (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1, AST.LiteralReal rspC 2.0, AST.LiteralBoolean rspC True])
              (insertIdentifier "ff" (DProcedure [TInteger, TReal, TBoolean]) emptyScope) `shouldBe`
            []
          it "ff(1, 2.0, TRUE) [ff -> Procedure [Real, Boolean, Integer]] returns (Integer, 3*errors)" $
            tcStatement
              (AST.Call (AST.Identifier rspB "ff") [AST.LiteralInteger rspC 1, AST.LiteralReal rspC 2.0, AST.LiteralBoolean rspC True])
              (insertIdentifier "ff" (DProcedure [TReal, TBoolean, TInteger]) emptyScope) `shouldBe`
            [ Errors.ArgumentHasIncorrectType rspC "ff" 0 TReal TInteger
            , Errors.ArgumentHasIncorrectType rspC "ff" 1 TBoolean TReal
            , Errors.ArgumentHasIncorrectType rspC "ff" 2 TInteger TBoolean
            ]
          it "ff(x) [ff -> Procedure [^Integer], x -> Integer] returns no errors" $
            tcStatement
              (AST.Call (AST.Identifier rspB "ff") [AST.IdentifierReference (AST.Identifier rspC "x")])
              (insertIdentifier "ff" (DProcedure [TPointerTo TInteger]) $ insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            []
          it "ff(x) [ff -> Procedure [Integer] Integer, x -> Integer] returns no errors" $
            tcStatement
              (AST.Call (AST.Identifier rspB "ff") [AST.IdentifierReference (AST.Identifier rspC "x")])
              (insertIdentifier "ff" (DProcedure [TInteger]) $ insertStandardIdentifier "x" TInteger emptyScope) `shouldBe`
            []
      describe "Declartations" $ do
        describe "Variable" $ do
          it "VAR a, b : INTEGER [] results in [a -> Integer, b -> Integer]" $
            tcVariableDeclaration (AST.VariableDeclaration [AST.Identifier rspA "a", AST.Identifier rspA "b"] AST.Integer) emptyScope `shouldBe`
            (insertStandardIdentifier "a" TInteger $ insertStandardIdentifier "b" TInteger emptyScope, [])
          it "VAR a, b : REAL [] results in [a -> Real, b -> Real]" $
            tcVariableDeclaration (AST.VariableDeclaration [AST.Identifier rspA "a", AST.Identifier rspA "b"] AST.Real) emptyScope `shouldBe`
            (insertStandardIdentifier "a" TReal $ insertStandardIdentifier "b" TReal emptyScope, [])
          it "VAR a, b : BOOLEAN [] results in [a -> Boolean, b -> Boolean]" $
            tcVariableDeclaration (AST.VariableDeclaration [AST.Identifier rspA "a", AST.Identifier rspA "b"] AST.Boolean) emptyScope `shouldBe`
            (insertStandardIdentifier "a" TBoolean $ insertStandardIdentifier "b" TBoolean emptyScope, [])
          it "VAR a : BOOLEAN [a -> TInteger] results in IdentifierAlreadyInUse error" $
            tcVariableDeclaration (AST.VariableDeclaration [AST.Identifier rspA "a"] AST.Boolean) (insertStandardIdentifier "a" TBoolean emptyScope) `shouldBe`
            (insertStandardIdentifier "a" TBoolean emptyScope, [Errors.IdentifierAlreadyInUse rspA "a"])
        describe "Procedure" $ do
          it "PROCEDURE a(x : INTEGER; VAR y : INTEGER) [] results in [a -> PROCEDURE [Integer, ^Integer]]" $
            tcDeclaration
              (AST.ProcedureDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByValue [AST.Identifier rspB "x"] AST.Integer, AST.PassByReference [AST.Identifier rspB "y"] AST.Integer]
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DProcedure [TInteger, TPointerTo TInteger]) emptyScope, [])
          it "PROCEDURE a(x : INTEGER; VAR x : INTEGER) [] results in [a -> PROCEDURE [Integer, ^Integer]] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.ProcedureDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByValue [AST.Identifier rspB "x"] AST.Integer, AST.PassByReference [AST.Identifier rspB "x"] AST.Integer]
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DProcedure [TInteger, TPointerTo TInteger]) emptyScope, [Errors.IdentifierAlreadyInUse rspB "x"])
          it "PROCEDURE a(VAR x : INTEGER; x : INTEGER) [] results in [a -> PROCEDURE [^Integer, Integer]] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.ProcedureDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByReference [AST.Identifier rspB "x"] AST.Integer, AST.PassByValue [AST.Identifier rspB "x"] AST.Integer]
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DProcedure [TPointerTo TInteger, TInteger]) emptyScope, [Errors.IdentifierAlreadyInUse rspB "x"])
          it "PROCEDURE a() [a -> Integer] results in [a -> PROCEDURE []] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.ProcedureDeclaration (AST.Identifier rspA "a") [] (AST.Block [] [] []))
              (insertStandardIdentifier "a" TInteger emptyScope) `shouldBe`
            (insertStandardIdentifier "a" TInteger emptyScope, [Errors.IdentifierAlreadyInUse rspA "a"])
        describe "Function" $ do
          it "FUNCTION a(x : INTEGER; VAR y : INTEGER):INTEGER [] results in [a -> FUNCTION [Integer, ^Integer] INTEGER]" $
            tcDeclaration
              (AST.FunctionDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByValue [AST.Identifier rspB "x"] AST.Integer, AST.PassByReference [AST.Identifier rspB "y"] AST.Integer]
                 AST.Integer
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DFunction [TInteger, TPointerTo TInteger] TInteger) emptyScope, [])
          it
            "FUNCTION a(x : INTEGER; VAR x : INTEGER):INTEGER [] results in [a -> FUNCTION [Integer, ^Integer] Integer ] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.FunctionDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByValue [AST.Identifier rspB "x"] AST.Integer, AST.PassByReference [AST.Identifier rspB "x"] AST.Integer]
                 AST.Integer
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DFunction [TInteger, TPointerTo TInteger] TInteger) emptyScope, [Errors.IdentifierAlreadyInUse rspB "x"])
          it
            "FUNCTION a(VAR x : INTEGER; x : INTEGER):INTEGER [] results in [a -> FUNCTION [^Integer, Integer] INTEGER ] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.FunctionDeclaration
                 (AST.Identifier rspA "a")
                 [AST.PassByReference [AST.Identifier rspB "x"] AST.Integer, AST.PassByValue [AST.Identifier rspB "x"] AST.Integer]
                 AST.Integer
                 (AST.Block [] [] []))
              emptyScope `shouldBe`
            (insertIdentifier "a" (DFunction [TPointerTo TInteger, TInteger] TInteger) emptyScope, [Errors.IdentifierAlreadyInUse rspB "x"])
          it "FUNCTION a():INTEGER [a -> Integer] results in [a -> FUNCTION [] Integer] and IdentifierAlreadyInUse error" $
            tcDeclaration
              (AST.FunctionDeclaration (AST.Identifier rspA "a") [] AST.Integer (AST.Block [] [] []))
              (insertStandardIdentifier "a" TInteger emptyScope) `shouldBe`
            (insertStandardIdentifier "a" TInteger emptyScope, [Errors.IdentifierAlreadyInUse rspA "a"])
          describe "Result assignment" $ do
            it "FUNCTION a():INTEGER;BEGIN a := 1 [] END; reports no error" $
              tcDeclaration
                (AST.FunctionDeclaration
                   (AST.Identifier rspA "a")
                   []
                   AST.Integer
                   (AST.Block [] [] [AST.Assignment (AST.Identifier rspB "a") (AST.LiteralInteger rspB 1)]))
                emptyScope `shouldBe`
              (insertIdentifier "a" (DFunction [] TInteger) emptyScope, [])
            it "FUNCTION a():INTEGER;BEGIN a := 1.0 [] END; reports IncompatibleAssignment" $
              tcDeclaration
                (AST.FunctionDeclaration
                   (AST.Identifier rspA "a")
                   []
                   AST.Integer
                   (AST.Block [] [] [AST.Assignment (AST.Identifier rspB "a") (AST.LiteralReal rspB 1.0)]))
                emptyScope `shouldBe`
              (insertIdentifier "a" (DFunction [] TInteger) emptyScope, [Errors.IncompatibleAssignment rspB TInteger rspB TReal])
    describe "Compiler" $
      before_ (compileLibrary "pascalm.c") $ do
        it "Empty Program" $ crt "000"
        it "Hello World" $ crt "001"
        it "Operators" $ crt "002"
        it "Operators - integer divide (div) by 0" $ crt "002-001"
        it "Operators - integer modulo (mod) 0" $ crt "002-002"
        it "Operators - real divide ('/') by 0" $ crt "002-003"
        it "Operators - int divide ('/') by 0" $ crt "002-004"
        it "Global variable declaration" $ crt "003"
        it "If-Then statement" $ crt "004"
        it "If-Then-Else statement" $ crt "005"
        it "While statement" $ crt "006"
        it "Argumentless procedure definition and call" $ crt "007"
        it "Nested procedure definitions with invocation" $ crt "008"
        it "Nested procedure definitions with value arguments" $ crt "009"
        it "Nested procedure definitions with reference arguments" $ crt "010"
        it "Argumentless function definition and call" $ crt "011"
        it "Nested functions with value and reference arguments" $ crt "012"
        it "Recursive function" $ crt "013"

compileLibrary :: String -> IO ()
compileLibrary name = do
  (compileCode, compileResult) <- runCommand "./test/PascalM" "clang" ["-emit-llvm", "-c", "-S", "pascalm.c", "-o", "pascalm.ll"]
  if compileCode == 0
    then do
      (assembleCode, assembleResult) <- runCommand "./test/PascalM" "llvm-as" ["pascalm.ll", "-o", "pascalm.o"]
      if assembleCode == 0
        then return ()
        else putStrLn $ "Assembler error on pascalm library: " <> assembleResult
    else putStrLn $ "Compiler error on pascalm library: " <> compileResult

crt :: String -> IO ()
crt name = do
  content <- readFile $ "test/PascalM/" ++ name ++ ".pas"
  let source = extractSource content
  let output = extractOutput content
  output' <- cr source
  output' `shouldBe` output
  where
    extractSource content = intercalate "\n" $ takeWhile (not . isPrefixOf "--") $ dropWhile (isPrefixOf "--") $ lines content
    extractOutput content = intercalate "\n" $ drop 1 $ dropWhile (not . isPrefixOf "--") $ dropWhile (isPrefixOf "--") $ lines content
    cr content =
      case parse content of
        Left error -> return $ show error
        Right ast ->
          case tcProgram ast of
            ([], tst) -> do
              writeFile ("test/PascalM/" ++ name ++ ".ll") $ PP.render $ IR.pp $ compile tst
              (assembleCode, assembleResult) <- runCommand "./test/PascalM" "llvm-as" [name <> ".ll", "-o", name <> ".o"]
              if assembleCode == 0
                then do
                  (linkCode, linkResult) <- runCommand "./test/PascalM" "llvm-link" [name <> ".o", "pascalm.o", "-o", name <> ".bc"]
                  if linkCode == 0
                    then do
                      (_, runResult) <- runCommand "./test/PascalM" "lli" [name <> ".bc"]
                      return runResult
                    else return linkResult
                else return assembleResult
            (errors, _) -> return $ intercalate "\n" $ map show errors

runCommand :: String -> String -> [String] -> IO (Int, String)
runCommand directory name arguments = do
  (_, Just hout, Just herr, jHandle) <-
    Process.createProcess
      (Process.proc name arguments) {Process.cwd = Just directory, Process.std_out = Process.CreatePipe, Process.std_err = Process.CreatePipe}
  exitCode <- Process.waitForProcess jHandle
  stdOut <- IO.hGetContents hout
  stdErr <- IO.hGetContents herr
  case exitCode of
    ExitSuccess   -> return (0, stdOut <> stdErr)
    ExitFailure n -> return (n, stdOut <> stdErr)

tcProgram :: AST.Program -> ([Errors.Error], TST.Program)
tcProgram ast =
  let (tst, (_, _, _, errors')) = runState (typeCheckProgram ast) (emptyScope, Nothing, [], [])
   in (reverse errors', tst)

tcExpr :: AST.Expression -> Scope -> (Maybe StandardType, [Errors.Error])
tcExpr e scope =
  let ((type', _), (scope', _, _, errors')) = runState (typeCheckExpression e) (scope, Nothing, [], [])
   in (type', reverse errors')

tcStatement :: AST.Statement -> Scope -> [Errors.Error]
tcStatement s scope =
  let (_, (scope', _, _, errors')) = runState (typeCheckStatement s) (scope, Nothing, [], [])
   in reverse errors'

tcVariableDeclaration :: AST.VariableDeclaration -> Scope -> (Scope, [Errors.Error])
tcVariableDeclaration s scope =
  let (scope', _, _, errors') = execState (typeCheckVariableDeclaration s) (scope, Nothing, [], [])
   in (scope', errors')

tcDeclaration :: AST.Declaration -> Scope -> (Scope, [Errors.Error])
tcDeclaration s scope =
  let (scope', _, _, errors') = execState (typeCheckDeclaration s) (scope, Nothing, [], [])
   in (scope', errors')

rspA :: AST.SourcePosition
rspA = AST.mkPosition (AST.SourcePoint 10 1 11) 0

rspB :: AST.SourcePosition
rspB = AST.mkPosition (AST.SourcePoint 11 2 12) 0

rspC :: AST.SourcePosition
rspC = AST.mkPosition (AST.SourcePoint 12 3 13) 0

rspAB :: AST.SourcePosition
rspAB = rspA `AST.combine` rspB

rspABC :: AST.SourcePosition
rspABC = rspA `AST.combine` rspB `AST.combine` rspC
