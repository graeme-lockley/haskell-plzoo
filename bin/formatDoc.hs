#!/usr/bin/env stack
-- stack --resolver lts-12.21 script

import Data.List (intercalate, isPrefixOf)
import qualified Data.Map.Strict as Map
import Text.XML.Light.Input
import Text.XML.Light.Lexer
import Text.XML.Light.Output
import Text.XML.Light.Types


type Handler =
  Map.Map String String -> Int -> IO ([Content])

type Handlers =
  Map.Map String Handler


handlers =
  Map.insert "_code_include" codeExtract' $
  Map.insert "_include" include' $
  Map.empty


main = do
  x <- readFile "docs/Template.html"
  let dom = parseXMLDoc x
  case dom of
    Nothing -> putStrLn "Error parsing"
    Just d' -> do
      result <- transform d'
      writeFile ".stack-work/Document.html" $ ppcElement (useExtraWhiteSpace False prettyConfigPP) result


transform :: Element -> IO(Element)
transform (Element qname attribs content line) = do
  transformedContent <- mapM transformContent content
  return $ Element qname attribs (concat transformedContent) line


transformContent :: Content -> IO([Content])
transformContent (Elem e@(Element n@(QName name Nothing Nothing) attribs content line)) =
  case Map.lookup name handlers of
    Nothing -> do
      transformedContent <- mapM transformContent content
      return [Elem $ (Element n attribs (concat transformedContent) line)]
    Just handler ->
      let
        attributes =
          Map.fromList $ map (\(Attr (QName n _ _) v) -> (n, v)) attribs
      in
        handler attributes 0
transformContent c =
  return [c]


codeExtract' :: Handler
codeExtract' attribs lineNo = do
  src <- attribValue "src" "_code_include" lineNo attribs
  search <- attribValue "search" "_code_include" lineNo attribs
  lang <- attribValue' "lang" "haskell" attribs

  content <- readFile src
  let extract = takeWhile (\line -> isPrefixOf search line || isPrefixOf " " line) $ dropWhile (\line -> not (isPrefixOf search line)) $ lines content
  let extractContent = map (\line -> Text (CData CDataText (line ++ "\n") Nothing)) extract
  return $
    [ Elem $ Element (QName "pre" Nothing Nothing) []
        [ Elem $ Element (QName "code" Nothing Nothing) [Attr (QName "class" Nothing Nothing) (lang ++ "-html")]
            extractContent
            Nothing
        ] Nothing
    ]


include' :: Handler
include' attribs lineNo = do
  src <- attribValue "src" "_include" lineNo attribs
  content <- readFile src
  let dom = parseXMLDoc content
  case dom of
    Nothing -> do
      putStrLn $ "Error: _include: " ++ show lineNo ++ ": " ++ "Error parsing content in " ++ src
      return []
    Just d' -> do
      result <- transform d'
      return [Elem result]


attribValue :: String -> String -> Int -> Map.Map String String -> IO (String)
attribValue name handlerName lineNo attribs = do
  let value = Map.lookup name attribs
  case value of
    Nothing -> do
      putStrLn $ "Error: " ++ handlerName ++ ": " ++ show lineNo ++ ": Required attribute not found: " ++ name
      return ""
    Just v ->
      return v


attribValue' :: String -> String -> Map.Map String String -> IO (String)
attribValue' name defaultValue attribs = do
  let value = Map.lookup name attribs
  case value of
    Nothing -> do
      return defaultValue
    Just v ->
      return v