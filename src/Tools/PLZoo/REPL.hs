module Tools.PLZoo.REPL
  ( Language(..)
  , repl
  ) where

import           Control.Monad.Trans
import           Data.List              (isPrefixOf)
import           System.Console.Repline

data Language =
  Language
    { prompt  :: String
    , execute :: String -> IO ()
    }

type Repl a = HaskelineT IO a

repl :: Language -> IO ()
repl (Language prompt execute) = evalRepl (pure prompt) (cmd execute) options (Just ':') (Word0 completer) ini

cmd :: (String -> IO ()) -> String -> Repl ()
cmd execute input = liftIO $ execute input

completer :: Monad m => WordCompleter m
completer n = do
  let names = []
  return $ filter (isPrefixOf n) names

options :: [(String, [String] -> Repl ())]
options = []

ini :: Repl ()
ini = liftIO $ putStrLn "Welcome!"
