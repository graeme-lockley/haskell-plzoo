module Lang.Expr.Errors
  ( Error(..)
  ) where

import           Lang.Expr.AST (SourcePos (..))

data Error
  = LexicalError SourcePos String
  | ParsingError SourcePos String
  | DivideByZero SourcePos
  deriving (Show)
