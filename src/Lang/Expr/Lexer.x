{
module Lang.Expr.Lexer
  ( Lexer
  , Token(..)
  , TokenClass(..)
  , evalLexer
  , lexer
  ) where

import Data.Char (ord)
import Data.Word (Word8)
import Control.Monad.Except (throwError)
import Control.Monad.State (StateT, evalStateT, get, put)

import Lang.Expr.AST (SourcePos(..))
import Lang.Expr.Errors (Error(..))
}

$digit = 0-9

tokens :-
  $white+       ;
  "--".*        ;
  $digit+       { mkToken $ TokenInt . read }
  \=            { mkToken' TokenEq }
  \+            { mkToken' TokenPlus }
  \-            { mkToken' TokenMinus }
  \*            { mkToken' TokenTimes }
  \/            { mkToken' TokenDiv }
  \(            { mkToken' TokenLParen }
  \)            { mkToken' TokenRParen }

{

data Token
  = Token SourcePos TokenClass
  deriving (Eq, Show)


data TokenClass
  = TokenInt Integer
  | TokenEq
  | TokenPlus
  | TokenMinus
  | TokenTimes
  | TokenDiv
  | TokenLParen
  | TokenRParen
  | TokenEOF
  deriving (Eq)


instance Show TokenClass where
  show (TokenInt i) = show i
  show TokenEq = "="
  show TokenPlus = "+"
  show TokenMinus = "-"
  show TokenTimes = "*"
  show TokenDiv = "/"
  show TokenLParen = "("
  show TokenRParen = ")"
  show TokenEOF = "<EOF>"


mkToken :: (String -> TokenClass) -> SourcePos -> String -> Token
mkToken f pos text =
  Token pos $ f text


mkToken' :: TokenClass -> SourcePos -> String -> Token
mkToken' c pos _ =
  Token pos c


type AlexInput =
  ( String      -- current input string
  , SourcePos   -- current position
  )


alexGetByte :: AlexInput -> Maybe (Word8, AlexInput)
alexGetByte ("", _) = Nothing
alexGetByte ((c:s), p) = Just (toEnum $ ord c, (s, alexMove p c))
  where
    alexMove :: SourcePos -> Char -> SourcePos
    alexMove (Point offset line column) '\t' = Point (offset + 1) line       (column + alex_tab_size - ((column - 1) `mod` alex_tab_size))
    alexMove (Point offset line column) '\n' = Point (offset + 1) (line + 1) 1
    alexMove (Point offset line column) _    = Point (offset + 1) line       (column + 1)


type Lexer a =
  StateT AlexInput (Either Error) a


evalLexer :: Lexer a -> AlexInput -> Either Error a
evalLexer = evalStateT


readToken :: Lexer Token
readToken = do
  s <- get
  case alexScan s 0 of
    AlexEOF ->
      return $ Token (snd s) TokenEOF
    AlexError (c, p) ->
      throwError $ LexicalError p $ show $ take 1 c
    AlexSkip inp' _ -> do
      put inp'
      readToken
    AlexToken inp' len tk -> do
      put inp'
      return $ tk (snd s) $ take len $ fst s


lexer :: (Token -> Lexer a) -> Lexer a
lexer cont = readToken >>= cont

}
