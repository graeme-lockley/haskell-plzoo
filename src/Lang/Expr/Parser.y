{
module Lang.Expr.Parser
  ( parse
  ) where

import Control.Monad.Error (throwError)

import Lang.Expr.AST (Expr(..), SourcePos(..))
import Lang.Expr.Errors (Error(..))
import Lang.Expr.Lexer (Lexer, Token(..), TokenClass(..), evalLexer, lexer)
}

%name parseExp
%monad { Lexer }
%lexer { lexer }{ Token _ TokenEOF }
%tokentype { Token }
%error { parseError }

%token
  int { Token _ (TokenInt $$) }
  '=' { Token _ TokenEq }
  '+' { Token _ TokenPlus }
  '-' { Token _ TokenMinus }
  '*' { Token _ TokenTimes }
  '/' { Token $$ TokenDiv }
  '(' { Token _ TokenLParen }
  ')' { Token _ TokenRParen }

%left '+' '-'
%left '*' '/'
%left NEG

%%

Exp
  : Exp '+' Exp            { Plus $1 $3 }
  | Exp '-' Exp            { Minus $1 $3 }
  | Exp '*' Exp            { Times $1 $3 }
  | Exp '/' Exp            { Div $1 $2 $3 }
  | '(' Exp ')'            { $2 }
  | '-' Exp %prec NEG      { Negate $2 }
  | int                    { Int $1 }

{

parseError :: Token -> Lexer a
parseError (Token p c) =
  throwError $ ParsingError p $ show c


parse :: String -> Either Error Expr
parse input =
  evalLexer parseExp (input, Point 0 1 1)

}
