module Lang.Expr.Eval
  ( eval
  ) where

import           Control.Monad.Except
import           Lang.Expr.AST        (Expr (..), SourcePos (..))
import           Lang.Expr.Errors     (Error (..))

eval :: Expr -> Either Error Integer
eval (Int v) = return v
eval (Plus e1 e2) = do
  l <- eval e1
  r <- eval e2
  return $ l + r
eval (Minus e1 e2) = do
  l <- eval e1
  r <- eval e2
  return $ l - r
eval (Times e1 e2) = do
  l <- eval e1
  r <- eval e2
  return $ l * r
eval (Div e1 pos e2) = do
  l <- eval e1
  r <- eval e2
  if r == 0
    then throwError $ DivideByZero pos
    else return $ l `div` r
eval (Negate e) = do
  r <- eval e
  return $ -r
