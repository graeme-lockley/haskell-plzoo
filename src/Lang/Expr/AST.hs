module Lang.Expr.AST
  ( Expr(..)
  , SourcePos(..)
  ) where

data Expr
  = Plus Expr Expr
  | Minus Expr Expr
  | Times Expr Expr
  | Div Expr SourcePos Expr
  | Negate Expr
  | Int Integer
  deriving (Show)

data SourcePos =
  Point
    { offset :: Int
    , line   :: Int
    , column :: Int
    }
  deriving (Eq, Show)
