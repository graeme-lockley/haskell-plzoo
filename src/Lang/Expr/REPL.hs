module Lang.Expr.REPL
  ( expr
  ) where

import           Lang.Expr.AST    (SourcePos (..))
import           Lang.Expr.Errors
import           Lang.Expr.Eval   (eval)
import           Lang.Expr.Parser
import           Tools.PLZoo.REPL (Language (..))

expr = Language "expr> " exec

exec :: String -> IO ()
exec input =
  case parse input >>= eval of
    Left e  -> printError e
    Right v -> print v

printError :: Error -> IO ()
printError (LexicalError pos m) = putStrLn $ "Lexical Error: " ++ showPos pos ++ ": " <> m
printError (ParsingError pos m) = putStrLn $ "Lexical Error: " ++ showPos pos ++ ": Unexpected " <> m
printError (DivideByZero pos) = putStrLn $ "Runtime Error: " ++ showPos pos ++ ": Divide by zero"

showPos :: SourcePos -> String
showPos (Point _ line column) = show line ++ "," ++ show column
