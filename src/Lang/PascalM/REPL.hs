module Lang.PascalM.REPL
  ( expr
  ) where

import           Lang.PascalM.Parser
import           Tools.PLZoo.REPL    (Language (..))

expr = Language "PascalM> " exec

exec :: String -> IO ()
exec input = print $ parse input
