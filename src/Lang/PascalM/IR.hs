module Lang.PascalM.IR where

import qualified Data.ByteString.UTF8   as BSU
import qualified Data.Char              as Char
import qualified Data.Serialize.IEEE754 as IEEE754
import           Data.Serialize.Put     (runPut)
import           GHC.Float              (float2Double)
import           Prelude                hiding ((<>))
import qualified Prelude                as P
import           Text.PrettyPrint

data Module =
  Module [GlobalValue] [MetaData]
  deriving (Show)

data Type
  = VoidT
  | IT Int
  | FloatT
  | PointerT Type
  | ArrayT Int Type
  | StructT [Type]
  | ReferenceT String
  deriving (Eq, Show)

i64 = IT 64

i32 = IT 32

i8 = IT 8

i1 = IT 1

data GlobalValue
  = GlobalVariable String TypedValue
  | GlobalType String Type
  | GlobalFunction String Type [Type] [Instruction]
  | GlobalConstantString String String
  | GlobalDeclaration String Type [Type]
  deriving (Show)

data MetaData
  = NamedMetaData
  | UnnamedMetaData
  deriving (Show)

data Argument =
  Argument Type String
  deriving (Show)

data Instruction
  = IRetValue Type String
  | IRetVoid
  | ICall Type String [TypedValue]
  | ICallFun String Type String [TypedValue]
  | IAdd String Type TypedValue TypedValue
  | ISub String Type TypedValue TypedValue
  | IMul String Type TypedValue TypedValue
  | IFAdd String Type TypedValue TypedValue
  | IFSub String Type TypedValue TypedValue
  | IFMul String Type TypedValue TypedValue
  | IFNeg String Type TypedValue
  | IICmp String ICompareOP Type TypedValue TypedValue
  | IFCmp String FCompareOP Type TypedValue TypedValue
  | ICondBranch TypedValue String String
  | IBranch String
  | ILabel String
  | IPhi String Type [(TypedValue, String)]
  | ILoad String Type TypedValue
  | IStore TypedValue TypedValue
  | IAlloca String Type
  | IAssign String TypedValue
  | ISExt String TypedValue Type
  | IComment String
  deriving (Show)

data TypedValue
  = ConstantVoidTV
  | LiteralIntTV Type Int
  | LiteralRealTV Float
  | VariableTV Type String
  | GetElementPtrTV Type Bool Type TypedValue [TypedValue]
  deriving (Show)

typeOf :: TypedValue -> Type
typeOf ConstantVoidTV              = VoidT
typeOf (LiteralIntTV t _)          = t
typeOf (LiteralRealTV _)           = FloatT
typeOf (VariableTV t _)            = t
typeOf (GetElementPtrTV t _ _ _ _) = t

data ICompareOP
  = ICEQ
  | ICNE
  | ICLT
  | ICLE
  | ICGT
  | ICGE
  deriving (Show)

data FCompareOP
  = FCEQ
  | FCNE
  | FCLT
  | FCLE
  | FCGT
  | FCGE
  deriving (Show)

class PrettyPrintable x where
  pp :: x -> Doc

instance PrettyPrintable Module where
  pp (Module globals metaDatas) = vcat $ map (\g -> pp g $$ text " ") globals

instance PrettyPrintable GlobalValue where
  pp (GlobalVariable t v) = text t <+> text "= common global" <+> pp v <> text ", align 4"
  pp (GlobalType n t) = text n <+> text "=" <+> pp t
  pp (GlobalFunction n t args is) =
    let instructions =
          case is of
            [] -> [IRetVoid]
            _  -> is
     in text "define" <+> pp t <+> text n <> parens (intercalate (text ", ") args) <+> text "{" $$ nest 2 (vcat $ map pp instructions) $$ text "}"
  pp (GlobalConstantString n v) =
    let constantText = v
     in text n <+> text "= private unnamed_addr constant [" <> int (length v + 1) <+> text "x i8] c\"" <> text constantText <> text "\\00\", align 1"
  pp (GlobalDeclaration n t ts) = text "declare" <+> pp t <+> text n <+> parens (intercalate (text ", ") ts)

instance PrettyPrintable Type where
  pp VoidT          = text "void"
  pp (IT n)         = text "i" <> int n
  pp FloatT         = text "float"
  pp (PointerT t)   = pp t <> text "*"
  pp (ArrayT n t)   = text "[" <> int n <+> text "x" <+> pp t <> text "]"
  pp (StructT ts)   = text "type" <+> braces (intercalate (text ", ") ts)
  pp (ReferenceT n) = text n

instance PrettyPrintable Argument where
  pp (Argument t n) = pp t <+> text n

instance PrettyPrintable Instruction where
  pp (IRetValue t v) = text "ret" <+> pp t <+> text v
  pp IRetVoid = text "ret void"
  pp (ICall t n tvs) = text "call" <+> pp t <+> text n <> parens (intercalate (text ", ") tvs)
  pp (ICallFun v t n tvs) = text v <+> text "= call" <+> pp t <+> text n <> parens (intercalate (text ", ") tvs)
  pp (IAdd n t o1 o2) = text n <+> text "= add" <+> pp o1 <> text "," <+> ppValue o2
  pp (ISub n t o1 o2) = text n <+> text "= sub" <+> pp o1 <> text "," <+> ppValue o2
  pp (IMul n t o1 o2) = text n <+> text "= mul" <+> pp o1 <> text "," <+> ppValue o2
  pp (IFAdd n t o1 o2) = text n <+> text "= fadd" <+> pp o1 <> text "," <+> ppValue o2
  pp (IFSub n t o1 o2) = text n <+> text "= fsub" <+> pp o1 <> text "," <+> ppValue o2
  pp (IFMul n t o1 o2) = text n <+> text "= fmul" <+> pp o1 <> text "," <+> ppValue o2
  pp (IFNeg n t o) = text n <+> text "= fneg" <+> pp o
  pp (IICmp n op t o1 o2) = text n <+> text "= icmp" <+> pp op <+> pp t <+> ppValue o1 <> text "," <+> ppValue o2
  pp (IFCmp n op t o1 o2) = text n <+> text "= fcmp" <+> pp op <+> pp t <+> ppValue o1 <> text "," <+> ppValue o2
  pp (ICondBranch v trueL falseL) = text "br" <+> pp v <> text ", label %" <> text trueL <> text ", label %" <> text falseL
  pp (IBranch l) = text "br label %" <> text l
  pp (ILabel l) = text l <> text ":"
  pp (IPhi n t ops) =
    text n <+> text "= phi" <+> pp t <+> hcat (punctuate (text ", ") (map (\(v, l) -> text "[" <> ppValue v <> text ", %" <> text l <> text "]") ops))
  pp (ILoad n t v) = text n <+> text "= load" <+> pp t <> text "," <+> pp v
  pp (IStore v t) = text "store" <+> pp v <> text "," <+> pp t
  pp (IAlloca n t) = text n <+> text "= alloca" <+> pp t
  pp (IAssign n (GetElementPtrTV _ inbounds t tv tvs)) =
    text n <+>
    text "=" <+>
    text "getelementptr" <>
    (if inbounds
       then text " inbounds"
       else text "") <+>
    pp t <> text ", " <> pp tv <> hcat (map (\item -> text ", " <> pp item) tvs)
  pp (IAssign n v) = text n <+> ppValue v
  pp (ISExt n v t) = text n <+> text "= zext" <+> pp v <+> text "to" <+> pp t
  pp (IComment t) = text ";" <+> text t

instance PrettyPrintable TypedValue where
  pp ConstantVoidTV = text "void"
  pp (LiteralIntTV t v) = pp t <+> int v
  pp (LiteralRealTV v) =
    let x = IEEE754.putFloat64be $ float2Double v
        vv = runPut x
        vvs =
          BSU.foldr
            (\c a ->
               let cOrd = Char.ord c
                   cHex v =
                     if v < 10
                       then Char.chr (v + Char.ord '0')
                       else Char.chr (v - 10 + Char.ord 'A')
                in cHex (cOrd `div` 16 `mod` 16) : cHex (cOrd `mod` 16) : a)
            ""
            vv
     in text "float" <+> text (show v) -- text "0x" <> text vvs
  pp (VariableTV t n) = pp t <+> text n
  pp (GetElementPtrTV rt inbounds t tv tvs) =
    pp rt <+>
    text "getelementptr" <>
    (if inbounds
       then text " inbounds"
       else text "") <+>
    text "(" <> pp t <> text ", " <> pp tv <> hcat (map (\item -> text ", " <> pp item) tvs) <> text ")"

ppValue ConstantVoidTV = text "void"
ppValue (LiteralIntTV _ v) = int v
ppValue (VariableTV _ n) = text n
ppValue (LiteralRealTV v) = float v
ppValue (GetElementPtrTV _ inbounds t tv tvs) =
  text "getelementptr" <>
  (if inbounds
     then text " inbounds"
     else text "") <+>
  text "(" <> pp t <> text ", " <> pp tv <> hcat (map (\item -> text ", " <> pp item) tvs) <> text ")"

instance PrettyPrintable ICompareOP where
  pp ICEQ = text "eq"
  pp ICNE = text "ne"
  pp ICLT = text "ult"
  pp ICLE = text "ule"
  pp ICGT = text "ugt"
  pp ICGE = text "uge"

instance PrettyPrintable FCompareOP where
  pp FCEQ = text "oeq"
  pp FCNE = text "one"
  pp FCLT = text "olt"
  pp FCLE = text "ole"
  pp FCGT = text "ogt"
  pp FCGE = text "oge"

intercalate :: PrettyPrintable a => Doc -> [a] -> Doc
intercalate sep items = hcat $ punctuate sep $ map pp items
