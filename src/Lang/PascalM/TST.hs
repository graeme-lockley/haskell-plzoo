module Lang.PascalM.TST where

import           Lang.PascalM.Types

data Program =
  Program String Block
  deriving (Show)

data Block =
  Block [VariableDeclaration] [Declaration] [Statement]
  deriving (Show)

data VariableDeclaration =
  VariableDeclaration String StandardType
  deriving (Show)

data Declaration
  = ProcedureDeclaration String [(String, StandardType)] Block
  | FunctionDeclaration String [(String, StandardType)] StandardType Block
  deriving (Show)

data Statement
  = IfThen Expression Statement
  | IfThenElse Expression Statement Statement
  | Sequence [Statement]
  | While Expression Statement
  | Assignment LHS Expression
  | Call String [Expression]
  | Write [Expression] Bool
  | Empty
  deriving (Show)

data LHS
  = NameL String
  | PointerL String
  deriving (Show)

lhsName :: LHS -> String
lhsName (NameL n)    = n
lhsName (PointerL n) = n

data Expression
  = Binary StandardType Op Expression Expression
  | Function StandardType String [Expression]
  | IdentifierReference StandardType String
  | Dereference StandardType Expression
  | LiteralBoolean Bool
  | LiteralInteger Int
  | LiteralReal Float
  | LiteralString String
  | Not Expression
  | Negate Expression
  | Parenthesis Expression
  deriving (Show)

typeOf :: Expression -> StandardType
typeOf (Binary t _ _ _)          = t
typeOf (Function t _ _)          = t
typeOf (IdentifierReference t _) = t
typeOf (Dereference t _)         = t
typeOf (LiteralBoolean _)        = TBoolean
typeOf (LiteralInteger _)        = TInteger
typeOf (LiteralReal _)           = TReal
typeOf (LiteralString _)         = TString
typeOf (Not _)                   = TBoolean
typeOf (Negate e)                = typeOf e
typeOf (Parenthesis e)           = typeOf e

data Op
  = Div
  | Divide
  | Minus
  | Mod
  | Plus
  | Times
  | Equal
  | GreaterEqual
  | GreaterThan
  | LessEqual
  | LessThan
  | NotEqual
  | And
  | Or
  deriving (Show)
