module Lang.PascalM.AST where

data Program =
  Program Identifier Block
  deriving (Show)

data Block =
  Block [VariableDeclaration] [Declaration] [Statement]
  deriving (Show)

data VariableDeclaration =
  VariableDeclaration [Identifier] StandardType
  deriving (Show)

data Declaration
  = ProcedureDeclaration Identifier [Argument] Block
  | FunctionDeclaration Identifier [Argument] StandardType Block
  deriving (Show)

data StandardType
  = Real
  | Integer
  | Boolean
  | String
  deriving (Show)

data Argument
  = PassByValue [Identifier] StandardType
  | PassByReference [Identifier] StandardType
  deriving (Show)

data Statement
  = IfThen Expression Statement
  | IfThenElse Expression Statement Statement
  | Sequence [Statement]
  | While Expression Statement
  | Assignment Identifier Expression
  | Call Identifier [Expression]
  | Empty
  deriving (Show)

data Expression
  = ArithOp ArithOp Expression Expression
  | BoolOp BoolOp Expression Expression
  | CompOp CompOp Expression Expression
  | Function SourcePosition Identifier [Expression]
  | IdentifierReference Identifier
  | LiteralBoolean SourcePosition Bool
  | LiteralInteger SourcePosition Int
  | LiteralReal SourcePosition Float
  | LiteralString SourcePosition String
  | Not SourcePosition Expression
  | Negate SourcePosition Expression
  | Parenthesis SourcePosition Expression
  deriving (Show)

instance Positionable Expression where
  position (ArithOp _ e1 e2) = position e1 `combine` position e2
  position (BoolOp _ e1 e2) = position e1 `combine` position e2
  position (CompOp _ e1 e2) = position e1 `combine` position e2
  position (Function pos _ _) = pos
  position (IdentifierReference identifier) = position identifier
  position (LiteralBoolean pos _) = pos
  position (LiteralInteger pos _) = pos
  position (LiteralReal pos _) = pos
  position (LiteralString pos _) = pos
  position (Not pos _) = pos
  position (Negate pos _) = pos
  position (Parenthesis pos _) = pos

data ArithOp
  = Div
  | Divide
  | Minus
  | Mod
  | Plus
  | Times
  deriving (Show)

data CompOp
  = Equal
  | GreaterEqual
  | GreaterThan
  | LessEqual
  | LessThan
  | NotEqual
  deriving (Show)

data BoolOp
  = And
  | Or
  deriving (Show)

data Identifier =
  Identifier SourcePosition String
  deriving (Eq, Show)

instance Positionable Identifier where
  position (Identifier pos _) = pos

data SourcePosition =
  SourcePosition SourcePoint SourcePoint
  deriving (Eq)

instance Show SourcePosition where
  show (SourcePosition (SourcePoint o1 l1 c1) (SourcePoint o2 l2 c2))
    | o1 == o2 = show o1 <> " (" <> show l1 <> ", " <> show c1 <> ")"
    | l1 == l2 = show o1 <> "-" <> show o2 <> " (" <> show l1 <> ", " <> show c1 <> "-" <> show c2 <> ")"
    | otherwise = show o1 <> "-" <> show o2 <> " (" <> show l1 <> ", " <> show c1 <> ")-(" <> show l2 <> ", " <> show c2 <> ")"

mkPosition :: SourcePoint -> Int -> SourcePosition
mkPosition p 0 = SourcePosition p p
mkPosition p 1 = SourcePosition p p
mkPosition p@(SourcePoint offset line column) length = SourcePosition p (SourcePoint (offset + length') line (column + length'))
  where
    length' = length - 1

combine :: SourcePosition -> SourcePosition -> SourcePosition
combine (SourcePosition (SourcePoint oS1 lS1 cS1) (SourcePoint oE1 lE1 cE1)) (SourcePosition (SourcePoint oS2 lS2 cS2) (SourcePoint oE2 lE2 cE2)) =
  SourcePosition (SourcePoint (min oS1 oS2) (min lS1 lS2) (min cS1 cS2)) (SourcePoint (max oE1 oE2) (max lE1 lE2) (max cE1 cE2))

data SourcePoint =
  SourcePoint
    { offset :: Int
    , line   :: Int
    , column :: Int
    }
  deriving (Eq, Show)

class Positionable p where
  position :: p -> SourcePosition
