{
module Lang.PascalM.Parser
  ( parse
  ) where

import Control.Monad.Error (throwError)

import Lang.PascalM.AST
import Lang.PascalM.Errors (Error(..))
import Lang.PascalM.Lexer (Lexer, Token(..), evalLexer, lexer)
}

%name parseProgram
%monad { Lexer }
%lexer { lexer }{ TokenEOF _ }
%tokentype { Token }
%error { parseError }

%token
  and            { TokenAnd _ }
  begin          { TokenBegin _ }
  boolean        { TokenBoolean _ }
  div            { TokenDiv $$ }
  do             { TokenDo _ }
  else           { TokenElse _ }
  end            { TokenEnd _ }
  function       { TokenFunction _ }
  if             { TokenIf _ }
  integer        { TokenInteger _ }
  mod            { TokenMod $$ }
  not            { TokenNot $$ }
  or             { TokenOr _ }
  procedure      { TokenProcedure _ }
  program        { TokenProgram _ }
  real           { TokenReal _ }
  then           { TokenThen _ }
  var            { TokenVar _ }
  while          { TokenWhile _ }

  identifier     { TokenIdentifier $$ }
  literalFalse   { TokenFalse $$ }
  literalInteger { TokenLiteralInteger $$ }
  literalReal    { TokenLiteralReal $$ }
  literalString  { TokenLiteralString $$ }
  literalTrue    { TokenTrue $$ }

  ':'            { TokenColon _ }
  ':='           { TokenColonEqual _ }
  ','            { TokenComma _ }
  '/'            { TokenDivide $$ }
  '='            { TokenEq _ }
  '>='           { TokenGreaterEqual _ }
  '>'            { TokenGreaterThan _ }
  '<='           { TokenLessEqual _ }
  '<'            { TokenLessThan _ }
  '('            { TokenLParen $$ }
  '-'            { TokenMinus $$ }
  '#'            { TokenNotEqual _ }
  '+'            { TokenPlus _ }
  '.'            { TokenPeriod _ }
  ')'            { TokenRParen $$ }
  ';'            { TokenSemicolon _ }
  '*'            { TokenTimes _ }

%nonassoc '=' '#' '<' '<=' '>' '>='
%left '+' '-' or
%left '*' '/' div and
%left NEG NOT

%%

Program :: { Program }
  : program identifier ';' Block '.'                                              { Program $2 $4 }

Block :: { Block }
  : OptionalVarDeclarations ProcedureOrFunctionDeclarations begin Statements end  { Block $1 $2 $4 }

OptionalVarDeclarations :: { [VariableDeclaration] }
  : var VarDeclarations                                                           { $2 }
  |                                                                               { [] }

VarDeclarations :: { [VariableDeclaration] }
  : VarDeclaration ';' VarDeclarations                                            { $1 : $3 }
  | VarDeclaration ';'                                                            { [$1] }

VarDeclaration :: { VariableDeclaration }
  : Identifiers ':' StandardType                                                  { VariableDeclaration $1 $3 }

StandardType :: { StandardType }
  : real                                                                          { Real }
  | integer                                                                       { Integer }
  | boolean                                                                       { Boolean }

ProcedureOrFunctionDeclarations :: { [Declaration] }
  : ProcedureOrFunctionDeclaration ProcedureOrFunctionDeclarations                { $1 : $2 }
  |                                                                               { [] }

ProcedureOrFunctionDeclaration :: { Declaration }
  : procedure identifier OptionalArguments ';' Block ';'                          { ProcedureDeclaration $2 $3 $5 }
  | function identifier OptionalArguments ':' StandardType ';' Block ';'          { FunctionDeclaration $2 $3 $5 $7 }

Statements :: { [Statement] }
  : Statement ';' Statements                                                      { $1 : $3 }
  | Statement                                                                     { [$1] }

Identifiers :: { [Identifier] }
  : identifier ',' Identifiers                                                    { $1 : $3 }
  | identifier                                                                    { [$1] }

OptionalArguments :: { [Argument] }
  : '(' Arguments ')'                                                             { $2 }
  | '(' ')'                                                                       { [] }
  |                                                                               { [] }

Arguments :: { [Argument] }
  : Argument ';' Arguments                                                        { $1 : $3 }
  | Argument                                                                      { [ $1 ]}

Argument :: { Argument }
  : var Identifiers ':' StandardType                                              { PassByReference $2 $4 }
  | Identifiers ':' StandardType                                                  { PassByValue $1 $3 }

Statement :: { Statement }
  : if Expression then Statement                                                  { IfThen $2 $4 }
  | if Expression then Statement else Statement                                   { IfThenElse $2 $4 $6 }
  | begin Statements end                                                          { Sequence $2 }
  | while Expression do Statement                                                 { While $2 $4 }
  | identifier ':=' Expression                                                    { Assignment $1 $3 }
  | identifier OptionalParameters                                                 { Call $1 (snd $2) }
  |                                                                               { Empty }

Expression :: { Expression }
  : Expression '+' Expression                                                     { mkBinaryOp ArithOp Plus $1 $3 }
  | Expression '-' Expression                                                     { mkBinaryOp ArithOp Minus $1 $3 }
  | Expression '*' Expression                                                     { mkBinaryOp ArithOp Times $1 $3 }
  | Expression '/' Expression                                                     { mkBinaryOp ArithOp Divide $1 $3 }
  | Expression div Expression                                                     { mkBinaryOp ArithOp Div $1 $3 }
  | Expression mod Expression                                                     { mkBinaryOp ArithOp Mod $1 $3 }
  | Expression and Expression                                                     { mkBinaryOp BoolOp And $1 $3 }
  | Expression or Expression                                                      { mkBinaryOp BoolOp Or $1 $3 }
  | Expression '=' Expression                                                     { mkBinaryOp CompOp Equal $1 $3 }
  | Expression '#' Expression                                                     { mkBinaryOp CompOp NotEqual $1 $3 }
  | Expression '<' Expression                                                     { mkBinaryOp CompOp LessThan $1 $3 }
  | Expression '>' Expression                                                     { mkBinaryOp CompOp GreaterThan $1 $3 }
  | Expression '>=' Expression                                                    { mkBinaryOp CompOp GreaterEqual $1 $3 }
  | Expression '<=' Expression                                                    { mkBinaryOp CompOp LessEqual $1 $3 }
  | '(' Expression ')'                                                            { Parenthesis ($1 `combine` $3) $2 }
  | '-' Expression %prec NEG                                                      { Negate ($1 `combine` position $2) $2 }
  | not Expression %prec NOT                                                      { Not ($1 `combine` position $2) $2 }
  | literalInteger                                                                { LiteralInteger (fst $1) (snd $1) }
  | literalReal                                                                   { LiteralReal (fst $1) (snd $1) }
  | literalTrue                                                                   { LiteralBoolean $1 True }
  | literalFalse                                                                  { LiteralBoolean $1 False }
  | literalString                                                                 { LiteralString (fst $1) (snd $1) }
  | identifier                                                                    { IdentifierReference $1 }
  | identifier OptionalParameters                                                 { let pos = position $1 `combine` fst $2 in Function pos $1 (snd $2) }

OptionalParameters :: { (SourcePosition, [Expression]) }
  : '(' Parameters ')'                                                            { ($1 `combine` $3, $2) }
  | '(' ')'                                                                       { ($1 `combine` $2, []) }

Parameters :: { [Expression] }
  : Expression ',' Parameters                                                     { $1 : $3 }
  | Expression                                                                    { [ $1 ] }


{

mkBinaryOp ::
     (a -> Expression -> Expression -> Expression)
  -> a
  -> Expression
  -> Expression
  -> Expression
mkBinaryOp constructor operator e1 e2 = constructor operator e1 e2


parseError :: Token -> Lexer a
parseError t =
  throwError $ ParsingError (position t) $ show t


parse :: String -> Either Error Program
parse input =
  evalLexer parseProgram (input, SourcePoint 0 1 1)

}
