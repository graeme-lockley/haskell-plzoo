module Lang.PascalM.Types where

data StandardType
  = TReal
  | TBoolean
  | TInteger
  | TString
  | TPointerTo StandardType
  deriving (Eq, Show)

data DeclarationType
  = DStandard StandardType
  | DProcedure [StandardType]
  | DFunction [StandardType] StandardType
  deriving (Eq, Show)
