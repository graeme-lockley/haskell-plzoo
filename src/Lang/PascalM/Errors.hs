module Lang.PascalM.Errors
  ( Error(..)
  ) where

import           Lang.PascalM.AST   (SourcePoint (..), SourcePosition (..))
import           Lang.PascalM.Types

data Error
  = LexicalError SourcePoint String
  | ParsingError SourcePosition String
  | DivideByZero SourcePosition
  | UnknownIdentifier SourcePosition String
  | NotRequiresBooleanOperand SourcePosition StandardType
  | NegateRequiresNumericOperand SourcePosition StandardType
  | UnableToPerformDivOnRealOperands SourcePosition
  | UnableToPerformModOnRealOperands SourcePosition
  | ArithmeticOperandsMismatch SourcePosition StandardType StandardType
  | ArithmeticOperandsNotNumbers SourcePosition StandardType
  | BooleanOperandsMismatch SourcePosition StandardType StandardType
  | ComparisonOperandsMismatch SourcePosition StandardType StandardType
  | AttemptToCallVariableAsFunction SourcePosition String
  | AttemptToCallProcedureAsFunction SourcePosition String
  | IncorrectNumberOfArguments SourcePosition String Int Int
  | ArgumentHasIncorrectType SourcePosition String Int StandardType StandardType
  | IfExpressionNotBoolean SourcePosition StandardType
  | WhileExpressionNotBoolean SourcePosition StandardType
  | IncompatibleAssignment SourcePosition StandardType SourcePosition StandardType 
  | AttemptToAssignValueToFunction SourcePosition String
  | AttemptToAssignValueToProcedure SourcePosition String
  | AttemptToCallVariableAsProcedure SourcePosition String
  | AttemptToCallFunctionAsProcedure SourcePosition String
  | IdentifierAlreadyInUse SourcePosition String
  deriving (Eq, Show)
