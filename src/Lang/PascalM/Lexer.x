{
module Lang.PascalM.Lexer
  ( Lexer
  , Token(..)
  , evalLexer
  , lexer
  ) where

import Data.Char (ord)
import Data.Word (Word8)
import Control.Monad.Except (throwError)
import Control.Monad.State (StateT, evalStateT, get, put)

import Lang.PascalM.AST
import Lang.PascalM.Errors (Error(..))
}

$digit = 0-9
$alpha = [a-zA-Z]
$alphaDigit = [0-9a-zA-Z]

tokens :-
  $white+       ;
  "--".*        ;

  [aA][nN][dD]                          { mkToken' TokenAnd }
  [bB][eE][gG][iI][nN]                  { mkToken' TokenBegin }
  [bB][oO][oO][lL][eE][aA][nN]          { mkToken' TokenBoolean }
  [dD][iI][vV]                          { mkToken' TokenDiv }
  [dD][oO]                              { mkToken' TokenDo }
  [eE][lL][sS][eE]                      { mkToken' TokenElse }
  [eE][nN][dD]                          { mkToken' TokenEnd }
  [fF][aA][lL][sS][eE]                  { mkToken' TokenFalse }
  [fF][uU][nN][cC][tT][iI][oO][nN]      { mkToken' TokenFunction }
  [iI][fF]                              { mkToken' TokenIf }
  [iI][nN][tT][eE][gG][eE][rR]          { mkToken' TokenInteger }
  [mM][oO][dD]                          { mkToken' TokenMod }
  [nN][oO][tT]                          { mkToken' TokenNot }
  [oO][rR]                              { mkToken' TokenOr }
  [pP][rR][oO][cC][eE][dD][uU][rR][eE]  { mkToken' TokenProcedure }
  [pP][rR][oO][gG][rR][aA][mM]          { mkToken' TokenProgram }
  [rR][eE][aA][lL]                      { mkToken' TokenReal }
  [tT][hH][eE][nN]                      { mkToken' TokenThen }
  [tT][rR][uU][eE]                      { mkToken' TokenTrue }
  [vV][aA][rR]                          { mkToken' TokenVar }
  [wW][hH][iI][lL][eE]                  { mkToken' TokenWhile }

  $digit+                               { \p t -> TokenLiteralInteger (p, read t) }
  $digit+.$digit+                       { \p t -> TokenLiteralReal (p, read t) }
  \"[^\"]*\"                            { \p t -> TokenLiteralString (p, drop 1 $ take (length t - 1) t) }
  $alpha $alphaDigit*                   { \p t -> TokenIdentifier $ Identifier p t }

  \:                                    { mkToken' TokenColon }
  \:\=                                  { mkToken' TokenColonEqual }
  \,                                    { mkToken' TokenComma }
  \/                                    { mkToken' TokenDivide }
  \=                                    { mkToken' TokenEq }
  \>\=                                  { mkToken' TokenGreaterEqual }
  \>                                    { mkToken' TokenGreaterThan }
  \<\=                                  { mkToken' TokenLessEqual }
  \<                                    { mkToken' TokenLessThan }
  \(                                    { mkToken' TokenLParen }
  \-                                    { mkToken' TokenMinus }
  \#                                    { mkToken' TokenNotEqual }
  \.                                    { mkToken' TokenPeriod }
  \+                                    { mkToken' TokenPlus }
  \)                                    { mkToken' TokenRParen }
  \;                                    { mkToken' TokenSemicolon }
  \*                                    { mkToken' TokenTimes }

{

data Token
  = TokenAnd SourcePosition
  | TokenBegin SourcePosition
  | TokenBoolean SourcePosition
  | TokenDiv SourcePosition
  | TokenDo SourcePosition
  | TokenElse SourcePosition
  | TokenEnd SourcePosition
  | TokenFalse SourcePosition
  | TokenFunction SourcePosition
  | TokenIf SourcePosition
  | TokenInteger SourcePosition
  | TokenMod SourcePosition
  | TokenNot SourcePosition
  | TokenOr SourcePosition
  | TokenProcedure SourcePosition
  | TokenProgram SourcePosition
  | TokenReal SourcePosition
  | TokenThen SourcePosition
  | TokenTrue SourcePosition
  | TokenVar SourcePosition
  | TokenWhile SourcePosition

  | TokenIdentifier Identifier
  | TokenLiteralInteger (SourcePosition, Int)
  | TokenLiteralReal (SourcePosition, Float)
  | TokenLiteralString (SourcePosition, String)

  | TokenColon SourcePosition
  | TokenColonEqual SourcePosition
  | TokenComma SourcePosition
  | TokenDivide SourcePosition
  | TokenEq SourcePosition
  | TokenGreaterEqual SourcePosition
  | TokenGreaterThan SourcePosition
  | TokenLessEqual SourcePosition
  | TokenLessThan SourcePosition
  | TokenLParen SourcePosition
  | TokenMinus SourcePosition
  | TokenNotEqual SourcePosition
  | TokenPeriod SourcePosition
  | TokenPlus SourcePosition
  | TokenRParen SourcePosition
  | TokenSemicolon SourcePosition
  | TokenTimes SourcePosition

  | TokenEOF SourcePoint
  deriving (Eq)


instance Show Token where
  show (TokenAnd _) = "AND"
  show (TokenBegin _) = "BEGIN"
  show (TokenBoolean _) = "BOOLEAN"
  show (TokenDiv _) = "DIV"
  show (TokenDo _) = "DO"
  show (TokenElse _) = "ELSE"
  show (TokenEnd _) = "END"
  show (TokenFalse _) = "FALSE"
  show (TokenFunction _) = "FUNCTION"
  show (TokenIf _) = "IF"
  show (TokenInteger _) = "INTEGER"
  show (TokenMod _) = "MOD"
  show (TokenNot _) = "NOT"
  show (TokenOr _) = "OR"
  show (TokenProcedure _) = "PROCEDURE"
  show (TokenProgram _) = "PROGRAM"
  show (TokenReal _) = "REAL"
  show (TokenThen _) = "THEN"
  show (TokenTrue _) = "TRUE"
  show (TokenVar _) = "VAR"
  show (TokenWhile _) = "WHILE"

  show (TokenIdentifier (Identifier _ i)) = show i
  show (TokenLiteralInteger (_, i)) = show i
  show (TokenLiteralReal (_, i)) = show i
  show (TokenLiteralString (_, i)) = show i

  show (TokenColon _) = ":"
  show (TokenColonEqual _) = ":="
  show (TokenComma _) = ","
  show (TokenDivide _) = "/"
  show (TokenEq _) = "="
  show (TokenGreaterEqual _) = ">="
  show (TokenGreaterThan _) = ">"
  show (TokenLessEqual _) = "<="
  show (TokenLessThan _) = "<"
  show (TokenLParen _) = "("
  show (TokenMinus _) = "-"
  show (TokenNotEqual _) = "#"
  show (TokenPeriod _) = "."
  show (TokenPlus _) = "+"
  show (TokenRParen _) = ")"
  show (TokenSemicolon _) = ";"
  show (TokenTimes _) = "*"
  show (TokenEOF _) = "<EOF>"


instance Positionable Token where
  position (TokenAnd p) = p
  position (TokenBegin p) = p
  position (TokenBoolean p) = p
  position (TokenDiv p) = p
  position (TokenDo p) = p
  position (TokenElse p) = p
  position (TokenEnd p) = p
  position (TokenFalse p) = p
  position (TokenFunction p) = p
  position (TokenIf p) = p
  position (TokenInteger p) = p
  position (TokenMod p) = p
  position (TokenNot p) = p
  position (TokenOr p) = p
  position (TokenProcedure p) = p
  position (TokenProgram p) = p
  position (TokenReal p) = p
  position (TokenThen p) = p
  position (TokenTrue p) = p
  position (TokenVar p) = p
  position (TokenWhile p) = p

  position (TokenIdentifier (Identifier p _)) = p
  position (TokenLiteralInteger (p, _)) = p
  position (TokenLiteralReal (p, _)) = p
  position (TokenLiteralString (p, _)) = p

  position (TokenColon p) = p
  position (TokenColonEqual p) = p
  position (TokenComma p) = p
  position (TokenDivide p) = p
  position (TokenEq p) = p
  position (TokenGreaterEqual p) = p
  position (TokenGreaterThan p) = p
  position (TokenLessEqual p) = p
  position (TokenLessThan p) = p
  position (TokenLParen p) = p
  position (TokenMinus p) = p
  position (TokenNotEqual p) = p
  position (TokenPeriod p) = p
  position (TokenPlus p) = p
  position (TokenRParen p) = p
  position (TokenSemicolon p) = p
  position (TokenTimes p) = p
  position (TokenEOF p) = SourcePosition p p


mkToken' :: (SourcePosition -> Token) -> SourcePosition -> String -> Token
mkToken' f pos _ = f pos


type AlexInput =
  ( String        -- current input string
  , SourcePoint   -- current position
  )


alexGetByte :: AlexInput -> Maybe (Word8, AlexInput)
alexGetByte ("", _) = Nothing
alexGetByte ((c:s), p) = Just (toEnum $ ord c, (s, alexMove p c))
  where
    alexMove :: SourcePoint -> Char -> SourcePoint
    alexMove (SourcePoint offset line column) '\t' = SourcePoint (offset + 1) line       (column + alex_tab_size - ((column - 1) `mod` alex_tab_size))
    alexMove (SourcePoint offset line column) '\n' = SourcePoint (offset + 1) (line + 1) 1
    alexMove (SourcePoint offset line column) _    = SourcePoint (offset + 1) line       (column + 1)


type Lexer a =
  StateT AlexInput (Either Error) a


evalLexer :: Lexer a -> AlexInput -> Either Error a
evalLexer = evalStateT


readToken :: Lexer Token
readToken = do
  s <- get
  case alexScan s 0 of
    AlexEOF ->
      return $ TokenEOF $ snd s
    AlexError (c, p) ->
      throwError $ LexicalError p $ show $ take 1 c
    AlexSkip inp' _ -> do
      put inp'
      readToken
    AlexToken inp' len tk -> do
      put inp'
      return $ tk (mkPosition (snd s) len) $ take len $ fst s


lexer :: (Token -> Lexer a) -> Lexer a
lexer cont = readToken >>= cont

}
