module Lang.PascalM.Compiler where

import           Control.Monad
import           Control.Monad.State
import qualified Data.Char
import           Data.List           (intercalate)
import qualified Data.Map.Strict     as Map
import           Data.Traversable    (mapM)
import qualified Lang.PascalM.IR     as IR
import qualified Lang.PascalM.TST    as TST
import           Lang.PascalM.Types

data CS =
  CS
    { constantStringCounter :: Int
    , variableNameCounter   :: Int
    , globals               :: [IR.GlobalValue]
    , nameScope             :: [String]
    , bindingScope          :: [Map.Map String Binding]
    }

data Binding
  = ProcedureB [String]
  | FunctionB [String] Int Int
  | GlobalVariableB
  | LocalVariableB Int Int
  | ParameterB Int Int

type CompileState a = State CS a

compile :: TST.Program -> IR.Module
compile p =
  let CS csc vnc globals nameScope bindingScope =
        execState (program p) $ CS {constantStringCounter = 0, variableNameCounter = 1, globals = [], nameScope = [], bindingScope = [Map.empty]}
   in IR.Module globals []

program :: TST.Program -> CompileState ()
program (TST.Program _ block') = do
  instructions <- compileGlobalBlock block'
  addGlobalValue $ IR.GlobalFunction "@main" IR.i32 [] (instructions ++ [IR.IRetValue IR.i32 "0"])
  addGlobalValue $ IR.GlobalDeclaration "@_write_string" IR.VoidT [IR.PointerT IR.i8]
  addGlobalValue $ IR.GlobalDeclaration "@_write_ln" IR.VoidT []
  addGlobalValue $ IR.GlobalDeclaration "@_write_int" IR.VoidT [IR.i32]
  addGlobalValue $ IR.GlobalDeclaration "@_write_real" IR.VoidT [IR.FloatT]
  addGlobalValue $ IR.GlobalDeclaration "@_write_bool" IR.VoidT [IR.i32]
  addGlobalValue $ IR.GlobalDeclaration "@_int_div" IR.i32 [IR.i32, IR.i32]
  addGlobalValue $ IR.GlobalDeclaration "@_int_divide" IR.FloatT [IR.i32, IR.i32]
  addGlobalValue $ IR.GlobalDeclaration "@_int_mod" IR.i32 [IR.i32, IR.i32]
  addGlobalValue $ IR.GlobalDeclaration "@_real_divide" IR.FloatT [IR.FloatT, IR.FloatT]

compileGlobalBlock :: TST.Block -> CompileState [IR.Instruction]
compileGlobalBlock (TST.Block variables' declarations' statements') = do
  compileGlobalVariableDeclarations variables'
  addVariableBindings variables'
  addDeclarationNameBindings declarations'
  compileDeclarations declarations'
  resetVariableNameCounter 1
  statements statements'

compileLocalBlock :: TST.Block -> CompileState [IR.Instruction]
compileLocalBlock (TST.Block variables' declarations' statements') = do
  addVariableBindings variables'
  addDeclarationNameBindings declarations'
  compileDeclarations declarations'
  resetVariableNameCounter 6
  variablesInstructions <- mapM initialiseVariable $ zip [1 ..] variables'
  statementInstructions <- statements statements'
  return $ concat variablesInstructions ++ statementInstructions

initialiseVariable :: (Int, TST.VariableDeclaration) -> CompileState [IR.Instruction]
initialiseVariable (idx, TST.VariableDeclaration name typ) = do
  struct <- structNames
  let stateT = IR.ReferenceT $ structNameState struct
  let statePointerT = IR.PointerT stateT
  name <- newVariableStringName
  return
    [ IR.IAssign name $
      IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT "%3") [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
    , IR.IStore (typedValueOf typ) (IR.VariableTV (IR.PointerT (typeOf typ)) name)
    ]

addVariableBindings :: [TST.VariableDeclaration] -> CompileState ()
addVariableBindings bindings = do
  lexicalScope <- lexicalScopeDepth
  if lexicalScope == 0
    then mapM_ globalVariableBinding bindings
    else mapM_ (localVariableBinding lexicalScope) $ zip [1 ..] bindings
  where
    globalVariableBinding (TST.VariableDeclaration name _) = addBinding name GlobalVariableB
    localVariableBinding lexicalScope (idx, TST.VariableDeclaration name _) = addBinding name $ LocalVariableB lexicalScope idx

compileGlobalVariableDeclarations :: [TST.VariableDeclaration] -> CompileState ()
compileGlobalVariableDeclarations = mapM_ compileGlobalVariableDeclaration
  where
    compileGlobalVariableDeclaration :: TST.VariableDeclaration -> CompileState ()
    compileGlobalVariableDeclaration (TST.VariableDeclaration name typ) = addGlobalValue $ IR.GlobalVariable ("@" <> name) $ typedValueOf typ

compileDeclarations :: [TST.Declaration] -> CompileState ()
compileDeclarations = mapM_ compileDeclaration
  where
    compileDeclaration :: TST.Declaration -> CompileState ()
    compileDeclaration (TST.ProcedureDeclaration n parameters block'@(TST.Block variables _ _)) = do
      pushNameIntoScopeNamespace n
      emitProcedureArgsStruct parameters
      emitProcedureStateStruct variables
      addParameterBindings parameters
      instructions <- compileLocalBlock block'
      structNames <- structNames
      popScopeNamespace
      let argsPointerT = IR.PointerT $ IR.ReferenceT $ structNameArgs structNames
      let argsPointerPointerT = IR.PointerT argsPointerT
      let stateT = IR.ReferenceT $ structNameState structNames
      let statePointerT = IR.PointerT stateT
      addGlobalValue $
        IR.GlobalFunction
          ("@" <> intercalate "_" (map toLower structNames))
          IR.VoidT
          [argsPointerT]
          ([ IR.IAlloca "%2" argsPointerT
           , IR.IAlloca "%3" stateT
           , IR.IStore (IR.VariableTV argsPointerT "%0") (IR.VariableTV argsPointerPointerT "%2")
           , IR.ILoad "%4" argsPointerT (IR.VariableTV argsPointerPointerT "%2")
           , IR.IAssign "%5" $
             IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT "%3") [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
           , IR.IStore (IR.VariableTV argsPointerT "%4") (IR.VariableTV argsPointerPointerT "%5")
           ] ++
           instructions ++ [IR.IRetVoid])
    compileDeclaration (TST.FunctionDeclaration n parameters typ (TST.Block variables declarations statements')) = do
      let variables' = variables ++ [TST.VariableDeclaration n typ]
      let block' = TST.Block variables' declarations statements'
      pushNameIntoScopeNamespace n
      emitProcedureArgsStruct parameters
      emitProcedureStateStruct variables'
      addParameterBindings parameters

      addVariableBindings variables
      addDeclarationNameBindings declarations
      compileDeclarations declarations
      resetVariableNameCounter 6
      variablesInstructions <- mapM initialiseVariable $ zip [1 ..] variables'
      statementInstructions <- statements statements'

      returnPointer <- newVariableStringName
      returnValue <- newVariableStringName
      structNames <- structNames
      popScopeNamespace
      let argsPointerT = IR.PointerT $ IR.ReferenceT $ structNameArgs structNames
      let argsPointerPointerT = IR.PointerT argsPointerT
      let stateT = IR.ReferenceT $ structNameState structNames
      let statePointerT = IR.PointerT stateT
      addGlobalValue $
        IR.GlobalFunction
          ("@" <> intercalate "_" (map toLower structNames))
          (typeOf typ)
          [argsPointerT]
          ([ IR.IAlloca "%2" argsPointerT
           , IR.IAlloca "%3" stateT
           , IR.IStore (IR.VariableTV argsPointerT "%0") (IR.VariableTV argsPointerPointerT "%2")
           , IR.ILoad "%4" argsPointerT (IR.VariableTV argsPointerPointerT "%2")
           , IR.IAssign "%5" $
             IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT "%3") [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
           , IR.IStore (IR.VariableTV argsPointerT "%4") (IR.VariableTV argsPointerPointerT "%5")
           ] ++
           concat variablesInstructions ++
           statementInstructions ++
           [ IR.IAssign returnPointer $
             IR.GetElementPtrTV
               stateT
               True
               stateT
               (IR.VariableTV statePointerT "%3")
               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 $ length variables']
           , IR.ILoad returnValue (typeOf typ) $ IR.VariableTV (IR.PointerT (typeOf typ)) returnPointer
           , IR.IRetValue (typeOf typ) returnValue
           ])

emitProcedureArgsStruct :: [(String, StandardType)] -> CompileState ()
emitProcedureArgsStruct args = do
  structNames <- structNames
  let structFields = map (typeOf . snd) args
  let allStructFields =
        if length structNames > 1
          then (IR.PointerT $ IR.ReferenceT $ structNameState $ drop 1 structNames) : structFields
          else structFields
  addGlobalValue $ IR.GlobalType (structNameArgs structNames) $ IR.StructT allStructFields

emitProcedureStateStruct :: [TST.VariableDeclaration] -> CompileState ()
emitProcedureStateStruct variables = do
  structNames <- structNames
  addGlobalValue $
    IR.GlobalType (structNameState structNames) $
    IR.StructT $ (IR.PointerT $ IR.ReferenceT $ structNameArgs structNames) : map (\(TST.VariableDeclaration _ t) -> typeOf t) variables

addParameterBindings parameters = do
  depth <- lexicalScopeDepth
  mapM (\(idx, (n, _)) -> addBinding n $ ParameterB depth idx) $ zip [0 ..] parameters

addDeclarationNameBindings :: [TST.Declaration] -> CompileState ()
addDeclarationNameBindings declarations = do
  ns <- structNames
  forM_ declarations (includeDeclarationNameBinding ns)
  where
    includeDeclarationNameBinding ns (TST.ProcedureDeclaration n _ _) = addBinding n $ ProcedureB (n : ns)
    includeDeclarationNameBinding ns (TST.FunctionDeclaration n _ _ (TST.Block v' _ _)) = addBinding n $ FunctionB (n : ns) (1 + length ns) (1 + length v')

statements :: [TST.Statement] -> CompileState [IR.Instruction]
statements statements' = concat <$> mapM statement statements'

statement :: TST.Statement -> CompileState [IR.Instruction]
statement TST.Empty = return []
statement (TST.Write arguments newline) = do
  arguments' <-
    mapM
      (\a -> do
         (tv, instructions) <- expression a
         if IR.typeOf tv == IR.i1
           then do
             name <- newVariableStringName
             return $ instructions ++ [IR.ISExt name tv IR.i32, IR.ICall IR.VoidT (writeName tv) [IR.VariableTV IR.i32 name]]
           else return $ instructions ++ [IR.ICall IR.VoidT (writeName tv) [tv]])
      arguments
  let newlineInstruction = [IR.ICall IR.VoidT "@_write_ln" [] | newline]
  return $ concat arguments' ++ newlineInstruction
  where
    writeName tv =
      case IR.typeOf tv of
        IR.IT 1               -> "@_write_bool"
        IR.IT 32              -> "@_write_int"
        IR.PointerT (IR.IT 8) -> "@_write_string"
        IR.FloatT             -> "@_write_real"
        _                     -> "Error: Unable to produce _write" <> show tv
statement (TST.Assignment n e) = do
  binding <- lookupLHSBinding n
  (t, i) <- expression e
  case binding of
    Just GlobalVariableB -> return $ i ++ [IR.IStore t (IR.VariableTV (IR.PointerT (IR.typeOf t)) ("@" <> TST.lhsName n))]
    Just (FunctionB _ variableLexicalScope idx) ->
      let localVariableAssignment :: String -> [String] -> CompileState [IR.Instruction]
          localVariableAssignment name names =
            if length names == variableLexicalScope
              then do
                let typ = IR.typeOf t
                let typPointer = IR.PointerT typ
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                namePointer <- newVariableStringName
                return
                  [ IR.IAssign namePointer $
                    IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
                  , IR.IStore t $ IR.VariableTV typPointer namePointer
                  ]
              else do
                let namesSuffix = drop 1 names
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                let argsT = IR.ReferenceT $ structNameArgs names
                let argsPointerT = IR.PointerT argsT
                let argsPointerPointerT = IR.PointerT argsPointerT
                let stateParentT = IR.ReferenceT $ structNameState namesSuffix
                let stateParentPointerT = IR.PointerT stateParentT
                let stateParentPointerPointerT = IR.PointerT stateParentPointerT
                let argsParentT = IR.ReferenceT $ structNameArgs namesSuffix
                let argsParentPointerT = IR.PointerT argsParentT
                let argsParentPointerPointerT = IR.PointerT argsParentPointerT
                stateName <- newVariableStringName
                argsName <- newVariableStringName
                stateParentName <- newVariableStringName
                argsParentName <- newVariableStringName
                superInstructions <- localVariableAssignment argsParentName namesSuffix
                return $
                  [ IR.IAssign stateName $
                    IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsName argsPointerT $ IR.VariableTV argsPointerPointerT stateName
                  , IR.IAssign stateParentName $
                    IR.GetElementPtrTV argsT True argsT (IR.VariableTV argsPointerT argsName) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsParentName stateParentPointerT $ IR.VariableTV stateParentPointerPointerT stateParentName
                  ] ++
                  superInstructions
       in do struct <- structNames
             i' <- localVariableAssignment "%3" struct
             return $ i ++ i'
    Just (LocalVariableB variableLexicalScope idx) ->
      let localVariableAssignment :: String -> [String] -> CompileState [IR.Instruction]
          localVariableAssignment name names =
            if length names == variableLexicalScope
              then do
                let typ = IR.typeOf t
                let typPointer = IR.PointerT typ
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                namePointer <- newVariableStringName
                return
                  [ IR.IAssign namePointer $
                    IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
                  , IR.IStore t $ IR.VariableTV typPointer namePointer
                  ]
              else do
                let namesSuffix = drop 1 names
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                let argsT = IR.ReferenceT $ structNameArgs names
                let argsPointerT = IR.PointerT argsT
                let argsPointerPointerT = IR.PointerT argsPointerT
                let stateParentT = IR.ReferenceT $ structNameState namesSuffix
                let stateParentPointerT = IR.PointerT stateParentT
                let stateParentPointerPointerT = IR.PointerT stateParentPointerT
                let argsParentT = IR.ReferenceT $ structNameArgs namesSuffix
                let argsParentPointerT = IR.PointerT argsParentT
                let argsParentPointerPointerT = IR.PointerT argsParentPointerT
                stateName <- newVariableStringName
                argsName <- newVariableStringName
                stateParentName <- newVariableStringName
                argsParentName <- newVariableStringName
                superInstructions <- localVariableAssignment argsParentName namesSuffix
                return $
                  [ IR.IAssign stateName $
                    IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsName argsPointerT $ IR.VariableTV argsPointerPointerT stateName
                  , IR.IAssign stateParentName $
                    IR.GetElementPtrTV argsT True argsT (IR.VariableTV argsPointerT argsName) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsParentName stateParentPointerT $ IR.VariableTV stateParentPointerPointerT stateParentName
                  ] ++
                  superInstructions
       in do struct <- structNames
             i' <- localVariableAssignment "%3" struct
             return $ i ++ i'
    Just (ParameterB variableLexicalScope idx) ->
      let dereferenceParameter :: String -> [String] -> CompileState [IR.Instruction]
          dereferenceParameter name names =
            if length names == variableLexicalScope
              then do
                let typ = IR.typeOf t
                let typPointer = IR.PointerT typ
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                let argsT = IR.ReferenceT $ structNameArgs names
                let argsPointerT = IR.PointerT argsT
                let argsPointerPointerT = IR.PointerT argsPointerT
                argsPointer <- newVariableStringName
                argsValue <- newVariableStringName
                namePointer <- newVariableStringName
                case n of
                  TST.NameL _ ->
                    return
                      [ IR.IAssign argsPointer $
                        IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                      , IR.ILoad argsValue argsPointerT $ IR.VariableTV argsPointerPointerT argsPointer
                      , IR.IAssign namePointer $
                        IR.GetElementPtrTV
                          argsT
                          True
                          argsT
                          (IR.VariableTV argsPointerT argsValue)
                          [ IR.LiteralIntTV IR.i32 0
                          , IR.LiteralIntTV
                              IR.i32
                              (idx +
                               if variableLexicalScope == 1
                                 then 0
                                 else 1)
                          ]
                      , IR.IStore t $ IR.VariableTV typPointer namePointer
                      ]
                  TST.PointerL _ -> do
                    xx <- newVariableStringName
                    let typPointerPointer = IR.PointerT typPointer
                    return
                      [ IR.IAssign argsPointer $
                        IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                      , IR.ILoad argsValue argsPointerT $ IR.VariableTV argsPointerPointerT argsPointer
                      , IR.IAssign namePointer $
                        IR.GetElementPtrTV
                          argsT
                          True
                          argsT
                          (IR.VariableTV argsPointerT argsValue)
                          [ IR.LiteralIntTV IR.i32 0
                          , IR.LiteralIntTV
                              IR.i32
                              (idx +
                               if variableLexicalScope == 1
                                 then 0
                                 else 1)
                          ]
                      , IR.ILoad xx typPointer $ IR.VariableTV typPointerPointer namePointer
                      , IR.IStore t $ IR.VariableTV typPointer xx
                      ]
              else do
                let namesSuffix = drop 1 names
                let stateT = IR.ReferenceT $ structNameState names
                let statePointerT = IR.PointerT stateT
                let argsT = IR.ReferenceT $ structNameArgs names
                let argsPointerT = IR.PointerT argsT
                let argsPointerPointerT = IR.PointerT argsPointerT
                let stateParentT = IR.ReferenceT $ structNameState namesSuffix
                let stateParentPointerT = IR.PointerT stateParentT
                let stateParentPointerPointerT = IR.PointerT stateParentPointerT
                let argsParentT = IR.ReferenceT $ structNameArgs namesSuffix
                let argsParentPointerT = IR.PointerT argsParentT
                let argsParentPointerPointerT = IR.PointerT argsParentPointerT
                stateName <- newVariableStringName
                argsName <- newVariableStringName
                stateParentName <- newVariableStringName
                argsParentName <- newVariableStringName
                superInstructions <- dereferenceParameter argsParentName namesSuffix
                return $
                  [ IR.IAssign stateName $
                    IR.GetElementPtrTV stateT True stateT (IR.VariableTV statePointerT name) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsName argsPointerT $ IR.VariableTV argsPointerPointerT stateName
                  , IR.IAssign stateParentName $
                    IR.GetElementPtrTV argsT True argsT (IR.VariableTV argsPointerT argsName) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                  , IR.ILoad argsParentName stateParentPointerT $ IR.VariableTV stateParentPointerPointerT stateParentName
                  ] ++
                  superInstructions
       in do struct <- structNames
             i' <- dereferenceParameter "%3" struct
             return $ i ++ i'
statement (TST.IfThen e s) = do
  (et, ei) <- expression e
  labelTrue <- newLabelStringName
  es <- statement s
  labelContinue <- newLabelStringName
  return $ ei ++ [IR.ICondBranch et labelTrue labelContinue, IR.ILabel labelTrue] ++ es ++ [IR.IBranch labelContinue, IR.ILabel labelContinue]
statement (TST.IfThenElse e s1 s2) = do
  (et, ei) <- expression e
  labelTrue <- newLabelStringName
  es1 <- statement s1
  labelFalse <- newLabelStringName
  es2 <- statement s2
  labelContinue <- newLabelStringName
  return $
    ei ++
    [IR.ICondBranch et labelTrue labelFalse, IR.ILabel labelTrue] ++
    es1 ++ [IR.IBranch labelContinue, IR.ILabel labelFalse] ++ es2 ++ [IR.IBranch labelContinue, IR.ILabel labelContinue]
statement (TST.While e s) = do
  labelTop <- newLabelStringName
  (et, ei) <- expression e
  labelTrue <- newLabelStringName
  es <- statement s
  labelContinue <- newLabelStringName
  return $
    [IR.IBranch labelTop, IR.ILabel labelTop] ++
    ei ++ [IR.ICondBranch et labelTrue labelContinue, IR.ILabel labelTrue] ++ es ++ [IR.IBranch labelTop, IR.ILabel labelContinue]
statement (TST.Sequence s) = statements s
statement (TST.Call n es) = do
  binding <- lookupBinding n
  case binding of
    Just (ProcedureB ns) ->
      let x argsName = do
            struct <- structNames
            if null struct
              then return (0, [])
              else do
                let argsT = IR.ReferenceT $ structNameArgs ns
                let argsPT = IR.PointerT argsT
                let stateT = IR.ReferenceT $ structNameState struct
                let statePT = IR.PointerT stateT
                let statePPT = IR.PointerT statePT
                name <- newVariableStringName
                return
                  ( 1
                  , [ IR.IAssign name $
                      IR.GetElementPtrTV argsT True argsT (IR.VariableTV argsPT argsName) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                    , IR.IStore (IR.VariableTV statePT "%3") (IR.VariableTV statePPT name)
                    ])
       in do name <- newVariableStringName
             (startOfIndex, startAssignmentInstruction) <- x name
             es' <- mapM expression es
             let sna = structNameArgs ns
             let callType = IR.ReferenceT sna
             argumentAssignments <-
               mapM
                 (\(idx, tv) -> do
                    let gepType = IR.ReferenceT sna
                    let gepSource = IR.VariableTV (IR.PointerT callType) name
                    name <- newVariableStringName
                    return
                      [ IR.IAssign name $
                        IR.GetElementPtrTV (IR.typeOf tv) True gepType gepSource [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
                      , IR.IStore tv $ IR.VariableTV (IR.PointerT $ IR.typeOf tv) name
                      ]) $
               zip [startOfIndex ..] $ map fst es'
             return $
               [IR.IAlloca name callType] ++
               startAssignmentInstruction ++
               concatMap snd es' ++
               concat argumentAssignments ++ [IR.ICall IR.VoidT ("@" <> toLower (intercalate "_" ns)) [IR.VariableTV (IR.PointerT callType) name]]

expression :: TST.Expression -> CompileState (IR.TypedValue, [IR.Instruction])
expression (TST.LiteralString s) = do
  name <- newConstantStringName
  addGlobalValue $ IR.GlobalConstantString name s
  let arrayT = IR.ArrayT (length s + 1) IR.i8
  return
    ( IR.GetElementPtrTV
        (IR.PointerT IR.i8)
        True
        arrayT
        (IR.VariableTV (IR.PointerT arrayT) name)
        [IR.LiteralIntTV IR.i64 0, IR.LiteralIntTV IR.i64 0]
    , [])
expression (TST.LiteralInteger n) = return (IR.LiteralIntTV IR.i32 n, [])
expression (TST.LiteralReal r) = return (IR.LiteralRealTV r, [])
expression (TST.LiteralBoolean b) = return (IR.LiteralIntTV IR.i1 (booleanToInteger b), [])
  where
    booleanToInteger True  = 1
    booleanToInteger False = 0
expression (TST.Binary TBoolean TST.And e1 e2) = do
  (t1, i1) <- expression e1
  labelTrue <- newLabelStringName
  (t2, i2) <- expression e2
  labelFalse <- newLabelStringName
  labelContinue <- newLabelStringName
  name <- newVariableStringName
  return
    ( IR.VariableTV IR.i1 name
    , i1 ++
      [IR.ICondBranch t1 labelTrue labelFalse, IR.ILabel labelTrue] ++
      i2 ++
      [ IR.IBranch labelContinue
      , IR.ILabel labelFalse
      , IR.IBranch labelContinue
      , IR.IPhi name IR.i1 [(t2, labelTrue), (IR.LiteralIntTV IR.i1 0, labelFalse)]
      ])
expression (TST.Binary TBoolean TST.Or e1 e2) = do
  (t1, i1) <- expression e1
  labelTrue <- newLabelStringName
  labelFalse <- newLabelStringName
  (t2, i2) <- expression e2
  labelContinue <- newLabelStringName
  name <- newVariableStringName
  return
    ( IR.VariableTV IR.i1 name
    , i1 ++
      [IR.ICondBranch t1 labelTrue labelFalse, IR.ILabel labelTrue, IR.IBranch labelContinue, IR.ILabel labelFalse] ++
      i2 ++ [IR.IBranch labelContinue, IR.IPhi name IR.i1 [(IR.LiteralIntTV IR.i1 1, labelTrue), (t2, labelFalse)]])
expression (TST.Binary TBoolean TST.Equal e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCEQ
  | otherwise = integerRelopExpression e1 e2 IR.ICEQ
expression (TST.Binary TBoolean TST.NotEqual e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCNE
  | otherwise = integerRelopExpression e1 e2 IR.ICNE
expression (TST.Binary TBoolean TST.LessThan e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCLT
  | otherwise = integerRelopExpression e1 e2 IR.ICLT
expression (TST.Binary TBoolean TST.LessEqual e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCLE
  | otherwise = integerRelopExpression e1 e2 IR.ICLE
expression (TST.Binary TBoolean TST.GreaterThan e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCGT
  | otherwise = integerRelopExpression e1 e2 IR.ICGT
expression (TST.Binary TBoolean TST.GreaterEqual e1 e2)
  | TST.typeOf e1 == TReal = floatRelopExpression e1 e2 IR.FCGE
  | otherwise = integerRelopExpression e1 e2 IR.ICGE
expression (TST.Binary TInteger TST.Plus e1 e2) = compileBinaryOp IR.i32 e1 e2 (`IR.IAdd` IR.i32)
expression (TST.Binary TInteger TST.Minus e1 e2) = compileBinaryOp IR.i32 e1 e2 (`IR.ISub` IR.i32)
expression (TST.Binary TInteger TST.Times e1 e2) = compileBinaryOp IR.i32 e1 e2 (`IR.IMul` IR.i32)
expression (TST.Binary TInteger TST.Div e1 e2) = compileBinaryOp IR.i32 e1 e2 (\n t1 t2 -> IR.ICallFun n IR.i32 "@_int_div" [t1, t2])
expression (TST.Binary TInteger TST.Mod e1 e2) = compileBinaryOp IR.i32 e1 e2 (\n t1 t2 -> IR.ICallFun n IR.i32 "@_int_mod" [t1, t2])
expression (TST.Binary TReal TST.Plus e1 e2) = compileBinaryOp IR.FloatT e1 e2 (`IR.IFAdd` IR.FloatT)
expression (TST.Binary TReal TST.Minus e1 e2) = compileBinaryOp IR.FloatT e1 e2 (`IR.IFSub` IR.FloatT)
expression (TST.Binary TReal TST.Times e1 e2) = compileBinaryOp IR.FloatT e1 e2 (`IR.IFMul` IR.FloatT)
expression (TST.Binary TReal TST.Divide e1 e2) = compileBinaryOp IR.FloatT e1 e2 foldOp
  where
    foldOp n t1 t2 =
      let functionName =
            if IR.typeOf t1 == IR.i32
              then "@_int_divide"
              else "@_real_divide"
       in IR.ICallFun n IR.FloatT functionName [t1, t2]
expression (TST.Parenthesis e) = expression e
expression (TST.Not e) = do
  (t, i) <- expression e
  name <- newVariableStringName
  return (IR.VariableTV IR.i1 name, i ++ [IR.ISub name IR.i1 (IR.LiteralIntTV IR.i1 1) t])
expression (TST.Negate e) = do
  (t, i) <- expression e
  name <- newVariableStringName
  let typ = IR.typeOf t
  case typ of
    IR.FloatT -> return (IR.VariableTV typ name, i ++ [IR.IFNeg name typ t])
    _ -> return (IR.VariableTV typ name, i ++ [IR.ISub name typ (IR.LiteralIntTV typ 0) t])
expression (TST.IdentifierReference t n) =
  let typ = typeOf t
      typPointer = IR.PointerT typ
   in do binding <- lookupBinding n
         case binding of
           Just GlobalVariableB -> return (IR.VariableTV typ ("@" <> n), [])
           Just (LocalVariableB variableLexicalScope idx) ->
             let dereferenceLocalVariable :: String -> [String] -> CompileState (IR.TypedValue, [IR.Instruction])
                 dereferenceLocalVariable name names =
                   if length names == variableLexicalScope
                     then do
                       let stateT = IR.ReferenceT $ structNameState names
                       let statePointerT = IR.PointerT stateT
                       namePointer <- newVariableStringName
                       return
                         ( IR.VariableTV typ namePointer
                         , [ IR.IAssign namePointer $
                             IR.GetElementPtrTV
                               stateT
                               True
                               stateT
                               (IR.VariableTV statePointerT name)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
                           ])
                     else do
                       let namesSuffix = drop 1 names
                       let stateT = IR.ReferenceT $ structNameState names
                       let statePointerT = IR.PointerT stateT
                       let argsT = IR.ReferenceT $ structNameArgs names
                       let argsPointerT = IR.PointerT argsT
                       let argsPointerPointerT = IR.PointerT argsPointerT
                       let stateParentT = IR.ReferenceT $ structNameState namesSuffix
                       let stateParentPointerT = IR.PointerT stateParentT
                       let stateParentPointerPointerT = IR.PointerT stateParentPointerT
                       let argsParentT = IR.ReferenceT $ structNameArgs namesSuffix
                       let argsParentPointerT = IR.PointerT argsParentT
                       let argsParentPointerPointerT = IR.PointerT argsParentPointerT
                       stateName <- newVariableStringName
                       argsName <- newVariableStringName
                       stateParentName <- newVariableStringName
                       argsParentName <- newVariableStringName
                       (superName, superInstructions) <- dereferenceLocalVariable argsParentName namesSuffix
                       return
                         ( superName
                         , [ IR.IAssign stateName $
                             IR.GetElementPtrTV
                               stateT
                               True
                               stateT
                               (IR.VariableTV statePointerT name)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                           , IR.ILoad argsName argsPointerT $ IR.VariableTV argsPointerPointerT stateName
                           , IR.IAssign stateParentName $
                             IR.GetElementPtrTV
                               argsT
                               True
                               argsT
                               (IR.VariableTV argsPointerT argsName)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                           , IR.ILoad argsParentName stateParentPointerT $ IR.VariableTV stateParentPointerPointerT stateParentName
                           ] ++
                           superInstructions)
              in do struct <- structNames
                    dereferenceLocalVariable "%3" struct
           Just (ParameterB variableLexicalScope idx) ->
             let dereferenceParameter :: String -> [String] -> CompileState (IR.TypedValue, [IR.Instruction])
                 dereferenceParameter name names =
                   if length names == variableLexicalScope
                     then do
                       let stateT = IR.ReferenceT $ structNameState names
                       let statePointerT = IR.PointerT stateT
                       let argsT = IR.ReferenceT $ structNameArgs names
                       let argsPointerT = IR.PointerT argsT
                       let argsPointerPointerT = IR.PointerT argsPointerT
                       argsPointer <- newVariableStringName
                       argsValue <- newVariableStringName
                       namePointer <- newVariableStringName
                       return
                         ( IR.VariableTV typ namePointer
                         , [ IR.IAssign argsPointer $
                             IR.GetElementPtrTV
                               stateT
                               True
                               stateT
                               (IR.VariableTV statePointerT name)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                           , IR.ILoad argsValue argsPointerT $ IR.VariableTV argsPointerPointerT argsPointer
                           , IR.IAssign namePointer $
                             IR.GetElementPtrTV
                               argsT
                               True
                               argsT
                               (IR.VariableTV argsPointerT argsValue)
                               [ IR.LiteralIntTV IR.i32 0
                               , IR.LiteralIntTV
                                   IR.i32
                                   (idx +
                                    if variableLexicalScope == 1
                                      then 0
                                      else 1)
                               ]
                           ])
                     else do
                       let namesSuffix = drop 1 names
                       let stateT = IR.ReferenceT $ structNameState names
                       let statePointerT = IR.PointerT stateT
                       let argsT = IR.ReferenceT $ structNameArgs names
                       let argsPointerT = IR.PointerT argsT
                       let argsPointerPointerT = IR.PointerT argsPointerT
                       let stateParentT = IR.ReferenceT $ structNameState namesSuffix
                       let stateParentPointerT = IR.PointerT stateParentT
                       let stateParentPointerPointerT = IR.PointerT stateParentPointerT
                       let argsParentT = IR.ReferenceT $ structNameArgs namesSuffix
                       let argsParentPointerT = IR.PointerT argsParentT
                       let argsParentPointerPointerT = IR.PointerT argsParentPointerT
                       stateName <- newVariableStringName
                       argsName <- newVariableStringName
                       stateParentName <- newVariableStringName
                       argsParentName <- newVariableStringName
                       (superName, superInstructions) <- dereferenceParameter argsParentName namesSuffix
                       return
                         ( superName
                         , [ IR.IAssign stateName $
                             IR.GetElementPtrTV
                               stateT
                               True
                               stateT
                               (IR.VariableTV statePointerT name)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                           , IR.ILoad argsName argsPointerT $ IR.VariableTV argsPointerPointerT stateName
                           , IR.IAssign stateParentName $
                             IR.GetElementPtrTV
                               argsT
                               True
                               argsT
                               (IR.VariableTV argsPointerT argsName)
                               [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                           , IR.ILoad argsParentName stateParentPointerT $ IR.VariableTV stateParentPointerPointerT stateParentName
                           ] ++
                           superInstructions)
              in do struct <- structNames
                    dereferenceParameter "%3" struct
expression (TST.Dereference t e) = do
  (t', e') <- expression e
  name <- newVariableStringName
  let (IR.VariableTV (IR.PointerT t'') _) = t'
  return (IR.VariableTV t'' name, e' ++ [IR.ILoad name t'' t'])
expression (TST.Function t n es) = do
  binding <- lookupBinding n
  case binding of
    Just (FunctionB ns _ _) ->
      let x argsName = do
            struct <- structNames
            if length struct < 2
              then return (0, [])
              else do
                let argsT = IR.ReferenceT $ structNameArgs ns
                let argsPT = IR.PointerT argsT
                let stateT = IR.ReferenceT $ structNameState struct
                let statePT = IR.PointerT stateT
                let statePPT = IR.PointerT statePT
                name <- newVariableStringName
                return
                  ( 1
                  , [ IR.IAssign name $
                      IR.GetElementPtrTV argsT True argsT (IR.VariableTV argsPT argsName) [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 0]
                    , IR.IStore (IR.VariableTV statePT "%3") (IR.VariableTV statePPT name)
                    ])
       in do name <- newVariableStringName
             (startOfIndex, startAssignmentInstruction) <- x name
             es' <- mapM expression es
             let sna = structNameArgs ns
             let callType = IR.ReferenceT sna
             argumentAssignments <-
               mapM
                 (\(idx, tv) -> do
                    let gepType = IR.ReferenceT sna
                    let gepSource = IR.VariableTV (IR.PointerT callType) name
                    name <- newVariableStringName
                    return
                      [ IR.IAssign name $
                        IR.GetElementPtrTV (IR.typeOf tv) True gepType gepSource [IR.LiteralIntTV IR.i32 0, IR.LiteralIntTV IR.i32 idx]
                      , IR.IStore tv $ IR.VariableTV (IR.PointerT $ IR.typeOf tv) name
                      ]) $
               zip [startOfIndex ..] $ map fst es'
             resultName <- newVariableStringName
             return
               ( IR.VariableTV (typeOf t) resultName
               , [IR.IAlloca name callType] ++
                 startAssignmentInstruction ++
                 concatMap snd es' ++
                 concat argumentAssignments ++
                 [IR.ICallFun resultName (typeOf t) ("@" <> toLower (intercalate "_" ns)) [IR.VariableTV (IR.PointerT callType) name]])

floatRelopExpression e1 e2 op = compileBinaryOp IR.i1 e1 e2 (\n -> IR.IFCmp n op IR.FloatT)

integerRelopExpression e1 e2 op = compileBinaryOp IR.i1 e1 e2 (\n t1 t2 -> IR.IICmp n op (IR.typeOf t1) t1 t2)

typeOf :: StandardType -> IR.Type
typeOf TReal           = IR.FloatT
typeOf TBoolean        = IR.i1
typeOf TInteger        = IR.i32
typeOf (TPointerTo tt) = IR.PointerT $ typeOf tt

typedValueOf :: StandardType -> IR.TypedValue
typedValueOf TReal    = IR.LiteralRealTV 0.0
typedValueOf TBoolean = IR.LiteralIntTV IR.i1 0
typedValueOf TInteger = IR.LiteralIntTV IR.i32 0

compileBinaryOp ::
     IR.Type
  -> TST.Expression
  -> TST.Expression
  -> (String -> IR.TypedValue -> IR.TypedValue -> IR.Instruction)
  -> CompileState (IR.TypedValue, [IR.Instruction])
compileBinaryOp returnType e1 e2 foldOp = do
  (t1, i1) <- expression e1
  (t2, i2) <- expression e2
  name <- newVariableStringName
  return (IR.VariableTV returnType name, i1 ++ i2 ++ [foldOp name t1 t2])

addGlobalValue :: IR.GlobalValue -> CompileState ()
addGlobalValue value = do
  globals <- gets globals
  modify (\s -> s {globals = globals ++ [value]})

newConstantStringName :: CompileState String
newConstantStringName = do
  counter <- gets constantStringCounter
  let name = "@.str" <> show counter
  modify (\s -> s {constantStringCounter = counter + 1})
  return name

newVariableStringName :: CompileState String
newVariableStringName = do
  counter <- gets variableNameCounter
  let name = "%" <> show counter
  modify (\s -> s {variableNameCounter = counter + 1})
  return name

resetVariableNameCounter :: Int -> CompileState ()
resetVariableNameCounter i = modify (\s -> s {variableNameCounter = i})

newLabelStringName :: CompileState String
newLabelStringName = do
  counter <- gets variableNameCounter
  modify (\s -> s {variableNameCounter = counter + 1})
  return $ show counter

pushNameIntoScopeNamespace :: String -> CompileState ()
pushNameIntoScopeNamespace n = do
  ns <- gets nameScope
  bs <- gets bindingScope
  case bs of
    [] -> modify (\s -> s {nameScope = n : ns, bindingScope = [Map.empty]})
    b':bs' -> modify (\s -> s {nameScope = n : ns, bindingScope = b' : b' : bs})

addBinding :: String -> Binding -> CompileState ()
addBinding n b = do
  bindings <- gets bindingScope
  case bindings of
    []     -> return ()
    b':bs' -> modify (\s -> s {bindingScope = Map.insert n b b' : bs'})

lookupBinding :: String -> CompileState (Maybe Binding)
lookupBinding n = do
  bindings <- gets bindingScope
  case bindings of
    []   -> return Nothing
    b':_ -> return $ Map.lookup n b'

lookupLHSBinding :: TST.LHS -> CompileState (Maybe Binding)
lookupLHSBinding (TST.NameL n)    = lookupBinding n
lookupLHSBinding (TST.PointerL n) = lookupBinding n

popScopeNamespace :: CompileState ()
popScopeNamespace = do
  ns <- gets nameScope
  bs <- gets bindingScope
  case (ns, bs) of
    (_:ns', _:bs') -> modify (\s -> s {nameScope = ns', bindingScope = bs'})
    _              -> return ()

structNames :: CompileState [String]
structNames = gets nameScope

structNamePrefix' :: [String] -> String
structNamePrefix' ns = toLower $ "%struct." ++ intercalate "_" ns

structNameArgs :: [String] -> String
structNameArgs ns = structNamePrefix' ns ++ "_args"

structNameState :: [String] -> String
structNameState ns = structNamePrefix' ns ++ "_state"

toLower :: String -> String
toLower = map Data.Char.toLower

lexicalScopeDepth :: CompileState Int
lexicalScopeDepth = length <$> structNames
