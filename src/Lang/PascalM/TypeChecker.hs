module Lang.PascalM.TypeChecker where

import           Control.Monad.State

import           Data.Char           (toLower)
import           Data.Foldable       (for_)
import qualified Data.Map.Strict     as Map
import           Data.Traversable    (mapM)

import qualified Lang.PascalM.AST    as AST
import qualified Lang.PascalM.Errors as Errors
import qualified Lang.PascalM.TST    as TST
import           Lang.PascalM.Types

type Scope = Map.Map String DeclarationType

type TypeCheck a
   = State ( Scope -- Mapping from identifier to declaration - this can either be variables, arguments, functions or procedures
           , Maybe String -- If the enclosing scope is a function then this is the function's name.
           , [(Scope, Maybe String)] -- The enclosing scopes when we are dealing with nested function and procedure declarations
           , [Errors.Error] -- The reported errors stored in reverse order
            ) a

typeCheckProgram :: AST.Program -> TypeCheck TST.Program
typeCheckProgram (AST.Program (AST.Identifier _ id) block) = do
  block' <- typeCheckBlock block
  return $ TST.Program id block'

typeCheckBlock :: AST.Block -> TypeCheck TST.Block
typeCheckBlock (AST.Block variableDeclarations declarations statements) = do
  variableDeclarations' <- mapM typeCheckVariableDeclaration variableDeclarations
  declarations' <- mapM typeCheckDeclaration declarations
  statements' <- mapM typeCheckStatement statements
  return $ TST.Block (concat variableDeclarations') declarations' statements'

typeCheckVariableDeclaration :: AST.VariableDeclaration -> TypeCheck [TST.VariableDeclaration]
typeCheckVariableDeclaration (AST.VariableDeclaration identifiers t) = do
  let resolvedType = resolve t
  for_ identifiers (insertDeclarationIntoScope (DStandard $ resolve t))
  return $ map (\(AST.Identifier _ n) -> TST.VariableDeclaration n resolvedType) identifiers

typeCheckDeclaration :: AST.Declaration -> TypeCheck TST.Declaration
typeCheckDeclaration declaration =
  let resolveParameters :: [AST.Argument] -> [(String, StandardType)]
      resolveParameters = concatMap resolveParameter
      resolveParameter :: AST.Argument -> [(String, StandardType)]
      resolveParameter (AST.PassByValue parameterIdentifiers t) = zip (names parameterIdentifiers) $ repeat $ resolve t
      resolveParameter (AST.PassByReference parameterIdentifiers t) = zip (names parameterIdentifiers) $ repeat $ TPointerTo $ resolve t
      names = map (\(AST.Identifier _ n) -> n)
      typeCheckParameters :: [AST.Argument] -> TypeCheck ()
      typeCheckParameters parameters = for_ parameters typeCheckParameter
      typeCheckParameter :: AST.Argument -> TypeCheck ()
      typeCheckParameter parameter =
        case parameter of
          AST.PassByValue parameter' t -> typeCheckParameter' parameter' $ resolve t
          AST.PassByReference parameter' t -> typeCheckParameter' parameter' $ TPointerTo $ resolve t
        where
          typeCheckParameter' parameterIdentifiers t = for_ parameterIdentifiers (insertDeclarationIntoScope $ DStandard t)
      typeCheckInnerScope :: Maybe String -> [AST.Argument] -> AST.Block -> TypeCheck TST.Block
      typeCheckInnerScope functionName parameters block = do
        openScope functionName
        typeCheckParameters parameters
        block' <- typeCheckBlock block
        restoreScope
        return block'
   in case declaration of
        (AST.ProcedureDeclaration id@(AST.Identifier _ name) parameters block) -> do
          let resolvedParameters = resolveParameters parameters
          insertDeclarationIntoScope (DProcedure (map snd resolvedParameters)) id
          block' <- typeCheckInnerScope Nothing parameters block
          return $ TST.ProcedureDeclaration name resolvedParameters block'
        (AST.FunctionDeclaration id@(AST.Identifier _ name) parameters t block) -> do
          let resolvedParameters = resolveParameters parameters
          let resolvedReturnType = resolve t
          insertDeclarationIntoScope (DFunction (map snd resolvedParameters) resolvedReturnType) id
          block' <- typeCheckInnerScope (Just name) parameters block
          return $ TST.FunctionDeclaration name resolvedParameters resolvedReturnType block'

resolve :: AST.StandardType -> StandardType
resolve AST.Real    = TReal
resolve AST.Integer = TInteger
resolve AST.Boolean = TBoolean
resolve AST.String  = TString

typeCheckStatement :: AST.Statement -> TypeCheck TST.Statement
typeCheckStatement (AST.IfThen e s) = do
  (t, e') <- dereferenceTypeCheck e
  s' <- typeCheckStatement s
  case t of
    Nothing -> return TST.Empty
    Just TBoolean -> return $ TST.IfThen e' s'
    Just t' -> reportErrorS $ Errors.IfExpressionNotBoolean (AST.position e) t'
typeCheckStatement (AST.IfThenElse e s1 s2) = do
  (t, e') <- dereferenceTypeCheck e
  s1' <- typeCheckStatement s1
  s2' <- typeCheckStatement s2
  case t of
    Nothing -> return TST.Empty
    Just TBoolean -> return $ TST.IfThenElse e' s1' s2'
    Just t' -> reportErrorS $ Errors.IfExpressionNotBoolean (AST.position e) t'
typeCheckStatement (AST.While e s) = do
  (t, e') <- dereferenceTypeCheck e
  s' <- typeCheckStatement s
  case t of
    Nothing -> return TST.Empty
    Just TBoolean -> return $ TST.While e' s'
    Just t' -> reportErrorS $ Errors.WhileExpressionNotBoolean (AST.position e) t'
typeCheckStatement (AST.Assignment (AST.Identifier pos n) e) =
  let normalize (TPointerTo t) = t
      normalize t              = t
   in do identifierType <- lookupIdentifier n
         (t, e') <- dereferenceTypeCheck e
         case (identifierType, t) of
           (Nothing, _) -> reportErrorS $ Errors.UnknownIdentifier pos n
           (Just (DStandard (TPointerTo n')), Just t')
             | n' == t'' -> return $ TST.Assignment (TST.PointerL n) e'
             | otherwise -> reportErrorS $ Errors.IncompatibleAssignment pos n' (AST.position e) t''
             where t'' = normalize t'
           (Just (DStandard n'), Just t')
             | n' == t'' -> return $ TST.Assignment (TST.NameL n) e'
             | otherwise -> reportErrorS $ Errors.IncompatibleAssignment pos n' (AST.position e) t''
             where t'' = normalize t'
           (Just (DFunction _ ft), Just t') -> do
             isValidFunctionAssignment <- isEnclosingFunctionName n
             case isValidFunctionAssignment of
               True
                 | ft == t'' -> return $ TST.Assignment (TST.NameL n) e'
                 | otherwise -> reportErrorS $ Errors.IncompatibleAssignment pos ft (AST.position e) t''
               False -> reportErrorS $ Errors.AttemptToAssignValueToFunction pos n
             where t'' = normalize t'
           (Just (DProcedure _), _) -> reportErrorS $ Errors.AttemptToAssignValueToProcedure pos n
           _ -> return TST.Empty
typeCheckStatement (AST.Call (AST.Identifier pos' n) arguments) = do
  declaration <- lookupIdentifier n
  case declaration of
    Nothing
      | eqIgnoreCase n "Write" -> do
        arguments' <- mapM dereferenceTypeCheck arguments
        return $ TST.Write (map snd arguments') False
      | eqIgnoreCase n "WriteLn" -> do
        arguments' <- mapM dereferenceTypeCheck arguments
        return $ TST.Write (map snd arguments') True
      | otherwise -> reportErrorS $ Errors.UnknownIdentifier pos' n
    Just (DStandard _) -> reportErrorS $ Errors.AttemptToCallVariableAsProcedure pos' n
    Just (DFunction _ _) -> reportErrorS $ Errors.AttemptToCallFunctionAsProcedure pos' n
    Just (DProcedure parameters)
      | length arguments == length parameters -> do
        arguments' <- typeCheckArguments n parameters arguments
        return $ TST.Call n arguments'
      | otherwise -> reportErrorS $ Errors.IncorrectNumberOfArguments pos' n (length parameters) (length arguments)
typeCheckStatement (AST.Sequence s) = do
  s' <- mapM typeCheckStatement s
  return $ TST.Sequence s'
typeCheckStatement _ = return TST.Empty

typeCheckExpression :: AST.Expression -> TypeCheck (Maybe StandardType, TST.Expression)
typeCheckExpression (AST.LiteralBoolean _ v) = return (Just TBoolean, TST.LiteralBoolean v)
typeCheckExpression (AST.LiteralInteger _ v) = return (Just TInteger, TST.LiteralInteger v)
typeCheckExpression (AST.LiteralReal _ v) = return (Just TReal, TST.LiteralReal v)
typeCheckExpression (AST.LiteralString _ v) = return (Just TString, TST.LiteralString v)
typeCheckExpression (AST.IdentifierReference (AST.Identifier pos n)) = do
  identifierType <- lookupStandardIdentifier n
  case identifierType of
    Nothing -> reportError' $ Errors.UnknownIdentifier pos n
    Just t' -> return (Just $ TPointerTo t', TST.IdentifierReference (TPointerTo t') n)
typeCheckExpression (AST.Not pos e) = do
  (t', e') <- dereferenceTypeCheck e
  case t' of
    Nothing -> return (Nothing, e')
    Just TBoolean -> return (t', TST.Not e')
    Just someOrOtherType -> reportError' $ Errors.NotRequiresBooleanOperand pos someOrOtherType
typeCheckExpression (AST.Negate pos e) = do
  (eType, e') <- dereferenceTypeCheck e
  case eType of
    Nothing -> return (Nothing, e')
    Just TInteger -> return (eType, TST.Negate e')
    Just TReal -> return (eType, TST.Negate e')
    Just someOrOtherType -> reportError' $ Errors.NegateRequiresNumericOperand pos someOrOtherType
typeCheckExpression e@(AST.ArithOp op e1 e2) = do
  (t1, e1') <- dereferenceTypeCheck e1
  (t2, e2') <- dereferenceTypeCheck e2
  let op' =
        case op of
          AST.Div    -> TST.Div
          AST.Divide -> TST.Divide
          AST.Minus  -> TST.Minus
          AST.Mod    -> TST.Mod
          AST.Plus   -> TST.Plus
          AST.Times  -> TST.Times
  case (t1, t2) of
    (Just TInteger, Just TInteger) ->
      case op' of
        TST.Divide -> return (Just TReal, TST.Binary TReal op' e1' e2')
        _          -> return (Just TInteger, TST.Binary TInteger op' e1' e2')
    (Just TReal, Just TReal) ->
      case op' of
        TST.Div -> reportError' $ Errors.UnableToPerformDivOnRealOperands $ AST.position e
        TST.Mod -> reportError' $ Errors.UnableToPerformModOnRealOperands $ AST.position e
        _ -> return (Just TReal, TST.Binary TReal op' e1' e2')
    (Just t1', Just t2')
      | t1' == t2' -> reportError' $ Errors.ArithmeticOperandsNotNumbers (AST.position e) t1'
      | otherwise -> reportError' $ Errors.ArithmeticOperandsMismatch (AST.position e) t1' t2'
    _ -> return (Nothing, e1')
typeCheckExpression e@(AST.BoolOp op e1 e2) = do
  (t1, e1') <- dereferenceTypeCheck e1
  (t2, e2') <- dereferenceTypeCheck e2
  let op' =
        case op of
          AST.And -> TST.And
          AST.Or  -> TST.Or
  case (t1, t2) of
    (Just TBoolean, Just TBoolean) -> return (Just TBoolean, TST.Binary TBoolean op' e1' e2')
    (Just t1', Just t2') -> reportError' $ Errors.BooleanOperandsMismatch (AST.position e) t1' t2'
    _ -> return (Nothing, e1')
typeCheckExpression e@(AST.CompOp op e1 e2) = do
  (t1, e1') <- dereferenceTypeCheck e1
  (t2, e2') <- dereferenceTypeCheck e2
  let op' =
        case op of
          AST.Equal        -> TST.Equal
          AST.GreaterEqual -> TST.GreaterEqual
          AST.GreaterThan  -> TST.GreaterThan
          AST.LessEqual    -> TST.LessEqual
          AST.LessThan     -> TST.LessThan
          AST.NotEqual     -> TST.NotEqual
  case (t1, t2) of
    (Just t1', Just t2')
      | t1' == t2' -> return (Just TBoolean, TST.Binary TBoolean op' e1' e2')
      | otherwise -> reportError' $ Errors.ComparisonOperandsMismatch (AST.position e) t1' t2'
    _ -> return (Nothing, e1')
typeCheckExpression (AST.Function pos (AST.Identifier pos' n) arguments) = do
  declaration <- lookupIdentifier n
  case declaration of
    Nothing -> reportError' $ Errors.UnknownIdentifier pos' n
    Just (DStandard _) -> reportError' $ Errors.AttemptToCallVariableAsFunction pos' n
    Just (DProcedure _) -> reportError' $ Errors.AttemptToCallProcedureAsFunction pos' n
    Just (DFunction parameters r)
      | length arguments == length parameters -> do
        arguments' <- typeCheckArguments n parameters arguments
        return (Just r, TST.Function r n arguments')
      | otherwise -> do
        reportError $ Errors.IncorrectNumberOfArguments pos n (length parameters) (length arguments)
        return (Just r, TST.Function r n [])
typeCheckExpression (AST.Parenthesis pos e) = typeCheckExpression e

typeCheckArguments :: String -> [StandardType] -> [AST.Expression] -> TypeCheck [TST.Expression]
typeCheckArguments name = typeCheckArguments' 0
  where
    typeCheckArguments' :: Int -> [StandardType] -> [AST.Expression] -> TypeCheck [TST.Expression]
    typeCheckArguments' n (a:as) (p:ps) = do
      t <- typeCheckArgument n a p
      ts <- typeCheckArguments' (n + 1) as ps
      return $ t : ts
    typeCheckArguments' _ _ _ = return []
    typeCheckArgument :: Int -> StandardType -> AST.Expression -> TypeCheck TST.Expression
    typeCheckArgument n a p = do
      (p', e') <- typeCheckExpression p
      case p' of
        Nothing -> return e'
        Just p''
          | a == p'' -> return e'
          | TPointerTo a == p'' -> return $ TST.Dereference a e'
          | otherwise -> do
            reportError $ Errors.ArgumentHasIncorrectType (AST.position p) name n a p''
            return e'

dereferenceTypeCheck :: AST.Expression -> TypeCheck (Maybe StandardType, TST.Expression)
dereferenceTypeCheck e = do
  (t, e') <- typeCheckExpression e
  return $ resolve t e'
  where
    resolve t e =
      case t of
        Just (TPointerTo (TPointerTo t')) -> (Just t', TST.Dereference (TPointerTo t') $ TST.Dereference t' e)
        Just (TPointerTo t') -> (Just t', TST.Dereference t' e)
        _ -> (t, e)

emptyScope :: Map.Map String DeclarationType
emptyScope = Map.empty

lookupIdentifier :: String -> TypeCheck (Maybe DeclarationType)
lookupIdentifier name = do
  (scope, _, _, _) <- get
  return $ Map.lookup name scope

lookupStandardIdentifier :: String -> TypeCheck (Maybe StandardType)
lookupStandardIdentifier name = do
  identifier <- lookupIdentifier name
  case identifier of
    Just (DStandard t) -> return $ Just t
    _                  -> return Nothing

insertIdentifier :: String -> DeclarationType -> Scope -> Scope
insertIdentifier = Map.insert

insertStandardIdentifier :: String -> StandardType -> Scope -> Scope
insertStandardIdentifier n s = insertIdentifier n (DStandard s)

reportError :: Errors.Error -> TypeCheck ()
reportError error = modify (\(scope, functionName, history, errors) -> (scope, functionName, history, error : errors))

reportError' :: Errors.Error -> TypeCheck (Maybe a, TST.Expression)
reportError' error = do
  reportError error
  return (Nothing, TST.LiteralInteger 0)

reportErrorS :: Errors.Error -> TypeCheck TST.Statement
reportErrorS error = do
  reportError error
  return TST.Empty

insertDeclarationIntoScope :: DeclarationType -> AST.Identifier -> TypeCheck ()
insertDeclarationIntoScope declarationType (AST.Identifier pos name) = do
  declaration' <- lookupIdentifier name
  case declaration' of
    Nothing -> bind' name declarationType
    Just _  -> reportError $ Errors.IdentifierAlreadyInUse pos name
  where
    bind' :: String -> DeclarationType -> TypeCheck ()
    bind' name declaration =
      modify (\(scope, functionName, history, errors) -> (insertIdentifier name declaration scope, functionName, history, errors))

openScope :: Maybe String -> TypeCheck ()
openScope name = modify (\(scope, functionName, history, errors) -> (scope, name, (scope, functionName) : history, errors))

restoreScope :: TypeCheck ()
restoreScope = modify (\(_, _, (scope, functionName):history, errors) -> (scope, functionName, history, errors))

isEnclosingFunctionName :: String -> TypeCheck Bool
isEnclosingFunctionName name = do
  (_, functionName, _, _) <- get
  case functionName of
    Nothing    -> return False
    Just name' -> return $ name' == name

eqIgnoreCase :: String -> String -> Bool
eqIgnoreCase s1 s2 =
  let s1' = map toLower s1
      s2' = map toLower s2
   in s1' == s2'
