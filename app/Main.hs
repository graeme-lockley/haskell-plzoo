module Main where

--import           Lang.Expr.REPL   (expr)
import           Lang.PascalM.REPL (expr)
import           Tools.PLZoo.REPL  (repl)

main :: IO ()
main = repl expr
