# haskell-plzoo

A collection of languages that have been implemented in Haskell.  The purpose of this project is to collect and assemble a zoo of language specimens with their individual algorithms.

I am hoping that folk will be able to go through each implementation and touch, prod, inspect, use, clone and improve each of the different specimens.

Finally the style that I would like to use for each specimen is the same style that [Stephen Diehl](https://github.com/sdiehl) employed in his `not-yet-complete-but-not-holding-my-breath` book _Write You a Haskell: Building a modern functional compiler from first principles_.

Finally finally I very much enjoyed the site [The Programming Languages Zoo: A potpourri of programming languages](http://plzoo.andrej.com) which introduced me to the idea of a programming languages zoo.  I have returned to this site many times to look at a particular aspect and how it was coded in OCaml.

> I am really looking for an analogy that I can use for this project.  A zoo isn't really cool because the thought of people touching, prodding, interrogating, using and cloning the animals is just inappropriate.  Perhaps this should be more of a collection of germs but, even then, that doesn't seem savoury.  So at present I am at a loss but will need to think of something.
